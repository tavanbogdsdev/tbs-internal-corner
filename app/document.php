<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\DocumentsLevels;

class document extends Model
{
    protected $fillable = [
        'name',
        'path',
        'version',
        'confirmedDate',
        'owner',
        'registration_info',
        'number',
        'category_id',
        'secret_level',
        'sub_id'
    ];

    protected $appends = [
        'levels'
    ];

    public function getLevelsAttribute() {
        $levels = DocumentsLevels::where('document_id', $this->id)
            ->join('levels', 'levels.id', '=', 'documents_levels.levels_id')
            ->get();
            return $levels;
    }

    public function levels(){
        return $this->belongsToMany('App\Levels', 'documents_levels', 'document_id');
    }


    public function documents(){
        return $this->belongsToMany('App\document', 'documents_levels', 'levels_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CategoriesLevels;
use App\Levels;

class Category extends Model
{
    protected $fillable = [
        'name',
        'nickname',
        'is_mini_app',
        'is_nested',
        'is_time_order'
    ];

    protected $appends = [
        'levels'
    ];

    public function getLevelsAttribute() {
        $levels = CategoriesLevels::where('category_id', $this->id)
            ->join('levels', 'levels.id', '=', 'categories_levels.levels_id')
            ->get();
        return $levels;
    }

    public function levels() {
        return $this->belongsToMany('App\Levels', 'categories_levels', 'category_id');
    }

    public function categories() {
        return $this->belongsToMany('App\Category', 'categories_levels', 'levels_id');
    }
}

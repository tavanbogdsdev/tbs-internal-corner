<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersCategories extends Model
{
    protected $table = 'users_categories';

    protected $fillable = [
        'user_id',
        'category_id'
    ];
    
    public function users() {
        return $this->belongsTo('App\User', 'id', 'user_id');
    }

    public function categories() {
        return $this->belongsTo('App\Category', 'id', 'category_id');
    }
}

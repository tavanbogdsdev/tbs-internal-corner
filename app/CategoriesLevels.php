<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriesLevels extends Model
{
    protected $table = 'categories_levels';

    protected $fillable = [
        'category_id',
        'levels_id'
    ];
    
    public function categories() {
        return $this->belongsTo('App\Category', 'id', 'category_id');
    }

    public function levels() {
        return $this->belongsTo('App\Levels', 'id', 'levels_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentsLevels extends Model
{
    protected $table = 'documents_levels';

    protected $fillable = [
        'document_id',
        'levels_id'
    ];

    public function documents() {
        return $this->belongsTo('App\Docment', 'id', 'document_id');
    }

    public function levels(){
        return $this->belongsTo('App\Levels', 'id', 'levels_id');
    }
}

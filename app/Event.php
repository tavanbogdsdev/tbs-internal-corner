<?php

namespace App;
use App\Http\Controllers\Redirect;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'title',
        'start_date',
        'end_date',
        'category_id',
        'sub_id',
        'user_id'
    ];
}

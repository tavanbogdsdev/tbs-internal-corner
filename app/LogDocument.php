<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogDocument extends Model
{
    protected $fillable = [
        'comment',
        'document_id'
    ];
}

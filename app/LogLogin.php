<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogLogin extends Model
{
    protected $fillable = [
        'comment',
        'user_id'
    ];
}

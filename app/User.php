<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Departments;
use App\Jobs;
use App\Levels;
use App\Roles;
use App\UsersCategories;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username','name', 'password', 'lastname', 'phone_number', 'department_id', 'job_id', 'level_id', 'role_id', 'machine_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
        'department',
        'job',
        'level',
        'role',
        'categories'
    ];

    public function getDepartmentAttribute() {
        return Departments::find($this->department_id) ? Departments::find($this->department_id)->name : ' ';
    }

    public function getJobAttribute() {
        return Jobs::find($this->job_id) ? Jobs::find($this->job_id)->name : ' ';
    }

    public function getLevelAttribute() {
        return Levels::find($this->level_id) ? Levels::find($this->level_id)->name : ' ';
    }

    public function getRoleAttribute() {
        return Roles::find($this->role_id) ? Roles::find($this->role_id)->name : ' ';
    }

    public function getCategoriesAttribute() {
        $categories = UsersCategories::where('user_id', $this->id)
            ->join('categories', 'categories.id', '=', 'users_categories.category_id')
            ->get();
        return $categories;
    }

    public function categories() {
        return $this->belongsToMany('App\Category', 'users_categories', 'user_id');
    }

    public function users() {
        return $this->belongsToMany('App\User', 'users_categories', 'category_id');
    }


    public function department() {
        return $this->belongsTo('App\Departments', 'department_id', 'id');
    }

    public function job() {
        return $this->belongsTo('App\Jobs', 'job_id', 'id');
    }

    public function level() {
        return $this->belongsTo('App\levels', 'level_id', 'id');
    }

    public function role() {
        return $this->belongsTo('App\Roles', 'role_id', 'id');
    }

    // public function roles() {
    //     return $this->belongsToMany('App\Roles', 'role_user', 'user_id', 'role_id');
    // }

    // public function getRoleNameAttribute() {
    //     $roles = RoleUser::where('user_id', $this->id)
    //         ->join('roles', 'roles.id', '=', 'role_user.role_id')
    //         ->get();
    //         foreach($roles as $role) {
    //             $role = $role->name;
    //             return $role;
    //         }
    // }

}

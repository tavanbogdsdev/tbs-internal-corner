<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class act extends Model
{
    protected $fillable = [
        'name',
        'path',
        'document_id',
        'number',
        'version',
        'confirmedDate',
        'secret_level'
    ];
}

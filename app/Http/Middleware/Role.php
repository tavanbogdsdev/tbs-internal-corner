<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        // $roles = explode('^', $role);
        // if(!Auth::check())
        //     return redirect('login');

        // $user = Auth::user();

        // $r = array_search($user->role, $roles);
        // if($r >= '1'){
        //     return $next($request);
        // } else {
        //     return redirect('/');
        // }

        return $next($request);
    }
}

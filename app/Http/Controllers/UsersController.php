<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Rules\MatchOldPassword;

use Illuminate\Http\Request;
use App\User;
use App\Departments;
use App\Jobs;
use App\Levels;
use App\Roles;
use App\Category;
use App\UsersCategories;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role == 'admin') {
            $users = User::all();
            $usersCategories = DB::table('users_categories')
                ->select('users_categories.user_id', 'categories.name')
                ->join('categories', 'categories.id', '=', 'users_categories.category_id')
                ->get();
            $departments = Departments::all();
            $jobs = Jobs::all();
            $levels = Levels::all();    
            $roles = Roles::all();
            $categories_all = Category::all();
            $categories = DB::table('categories')
                ->select('categories.*')
                ->join('categories_levels', 'categories.id', '=', 'categories_levels.category_id')
                ->where('levels_id', '=', Auth::user()->level_id)
                ->where('is_mini_app', '=', 0)
                ->get();
            // return $users;
            $currentMenu = 9999999999;
            return view('users.index', compact('users', 'departments', 'jobs', 'levels', 'roles', 'categories', 'currentMenu', 'categories_all', 'usersCategories'));
        } else {
            return redirect('/');
        }
    }


    /**
     * Display a hello of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // public function hello()
    // {
    //     return redirect('/');
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->machine_id == 0) {
            return redirect('/')->with('error', 'Хүний нөөцийн ажилтанд хандана уу!');
        } else {
            $token = null;
            $data = array(
                'icID' => Auth::user()->id,
                'machineID' => Auth::user()->machine_id,
                'uname' => Auth::user()->lastname.".".Auth::user()->name
            );
            $tokenResponse = $this->getToken($data['icID']);
            if (isset($tokenResponse->message) && $tokenResponse->message == 'User Not found.') {
                $createUserResponse = $this->createAMSuser($data);
                if (isset($createUserResponse->message) && $createUserResponse->message == 'User was registered successfully!') {
                    $tokenResponse = $this->getToken($data['icID']);
                    if (isset($tokenResponse->accessToken)) {
                        $token = $tokenResponse->accessToken;
                    } else {
                        echo "Failed 1";
                    }
                } else {
                    print_r($createUserResponse);
                    echo "Failed 2";
                }
                // print_r($this->createAMSuser($data));
            } else {
                if (isset($tokenResponse->accessToken)) {
                    $token = $tokenResponse->accessToken;
                } else {
                    echo "Failed 3";
                }
            }
            // return redirect('http://192.168.8.82:3000')->withCookie(cookie()->forever('name', '..!..'));
            echo "<script>window.open('".'http://192.168.8.82:3000/home/?token='.$token."', '_blank')</script>";
            return Redirect::to('http://192.168.8.34:3000/home/?token=' . $token);
        }
    }




    private function getToken($id)
    {
        $client = new \GuzzleHttp\Client();
        try {
            $response = $client->get('localhost:8080/api/user/login/' . $id);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
        }
        $responseBodyAsString = $response->getBody()->getContents();
        return json_decode($responseBodyAsString);
    }

    private function createAMSuser($data)
    {
        $client = new \GuzzleHttp\Client();
        $url = "localhost:8080/api/user/create";
        try {
            $response = $client->post($url,  ['json' => $data]);
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $response = $e->getResponse();
        }
        $responseBodyAsString = $response->getBody()->getContents();
        return json_decode($responseBodyAsString);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::create([
            'username' => $request['username'],
            'name' => $request['name'],
            'lastname' => $request['lastname'],
            'phone_number' => $request['phone_number'],
            'department_id' => $request['department_id'],
            'job_id' => $request['job_id'],
            'level_id' => $request['level_id'],
            'role_id' => $request['role_id'],
            'password' => Hash::make($request['password']),
        ]);
        if (!empty($request->categories[0])) {
            User::find($user->id)->categories()->attach($request->categories);
        }

        return redirect('/users')->with('success', 'Хэрэглэгч амжилттай үүсгэлээ.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changeForm()
    {
        $categories = Category::all();
        $currentMenu = 9999999997;
        return view('auth.passwords.change', compact('categories', 'currentMenu'));
    }

    public function change(Request $request)
    {
        if ($request['new_password'] != $request['password_confirmation']) {
            return redirect('/user/password/change')->with('error', 'Баталгаажуулах нууц үг буруу байна!');
        }
        $request->validate([
            'password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'password_confirmation' => ['same:new_password'],
        ]);

        User::find(auth()->user()->id)->update(['password' => Hash::make($request->new_password)]);

        return redirect('/')->with('success', 'Нууц үг амжилттай солигдлоо.');
    }

    public function reset($id)
    {
        User::find($id)->update(['password' => Hash::make('qwerty123')]);

        return redirect('/users')->with('success', 'Нууц үг амжилттай reset хийлээ.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'username' => ['required', 'string', 'max:255'],
            'name' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'phone_number' => ['required', 'string', 'max:8'],
            'department_id' => ['required'],
            'job_id' => ['required'],
            'level_id' => ['required'],
            'role_id' => ['required'],
            'machine_id' => ['nullable', 'integer'],
        ]);

        UsersCategories::where('user_id', $id)->delete();
        if ($request->categories != ['0']) {
            User::find($id)->categories()->attach($request->categories);
        }

        User::whereId($id)->update($validatedData);
        return redirect('/users')->with('success', 'Хэрэглэгч амжилттай засварлалаа.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        UsersCategories::where('user_id', $id)->delete();
        return redirect('/users')->with('success', 'Хэрэглэгч амжилттай устгалаа.');
    }
}

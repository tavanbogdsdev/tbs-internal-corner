<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Levels;

class LevelController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Levels::create([
            'name' => $request['name']
        ]);
        return redirect('/settings')->with('success', 'Түвшин амжилттай нэмлээ.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
        ]);

        Levels::whereId($id)->update($data);
        return redirect('/settings')->with('success', 'Түвшин амжилттай засварлалаа.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $level = Levels::findOrFail($id);
        $level->delete();
        return redirect('/settings')->with('success', 'Түвшин амжилттай устгалаа.');
    }
}

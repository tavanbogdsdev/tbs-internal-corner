<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Levels;
use App\document;
use App\act;
use App\DocumentsLevels;
use App\LogDocument;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\sub_category;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $levels = Levels::all();
        return view('documents.index', compact( 'levels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('file')->store('documents');
        $doc = Document::create([
            'name' => $request['name'],
            'path' => $file,
            'version' => $request['version'] ? $request['version'] : '',
            'confirmedDate' => $request['confirmedDate'] ? $request['confirmedDate'] : '',
            'owner' => $request['owner'] ? $request['owner'] : '',
            'registration_info' => $request['registration_info'] ? $request['registration_info'] : '',
            'category_id' => $request['category_id'],
            'number' => $request['number'] ? $request['number'] : '',
            'secret_level' => $request['secret_level'] ? $request['secret_level'] : '',
            'sub_id' => $request['sub_id'] ? $request['sub_id'] : null
        ]);

        Document::find($doc->id)->levels()->attach($request->levels);

        LogDocument::create([
            'comment' => $request['comment'] ? $request['comment'] : 'Шинэ бичиг баримт',
            'document_id' => $doc->id
        ]);

        return redirect()->back()->with('success', ' Бичиг баримт амжилттай үүслээ.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $document = Document::find($id);
        $document->name = $request['name'];
        $document->version = $request['version'] ? $request['version'] : '';
        $document->confirmedDate = $request['confirmedDate'] ? $request['confirmedDate'] : '';
        $document->owner = $request['owner'] ? $request['owner'] : '';
        $document->registration_info = $request['registration_info'] ? $request['registration_info'] : '';
        $document->number = $request['number'] ? $request['number'] : '';
        $document->secret_level = $request['secret_level'];
        $document->sub_id = $request['sub_id'];
        $document->save();

        if($request->levels) {
            DocumentsLevels::where('document_id', $id)->delete();
            Document::find($document->id)->levels()->attach($request->levels);
        }

        LogDocument::create([
            'comment' => $request['comment'] ? $request['comment'] : 'Засварлав',
            'document_id' => $document->id
        ]);

        return redirect()->back()->with('success', 'Амжилттай засварлалаа.');
    }

    // Change file of document.
    public function changefile(Request $request, $id) {
        if($request->file) {
            $docu = Document::findOrFail($id);
            Storage::delete($docu->path);

            $filepath = $request->file('file')->store('documents');
            $docu->path = $filepath;
            $docu->save();
            return redirect()->back()->with('success', 'Амжилттай засварлалаа.');
        }else {
            return redirect()->back()->with('error', 'Амжилтгүй: Файл сонгоно уу!.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $file = Document::findOrFail($id)->path;
        Storage::delete($file);

        Document::destroy($id);
        DocumentsLevels::where('document_id', $id)->delete();

        return redirect()->back()->with('success', 'Амжилттай устгалаа.');
    }

    public function showActs($id) {
        try {
            $categories = DB::table('categories')
                ->select('categories.*')
                ->join('categories_levels', 'categories.id', '=', 'categories_levels.category_id')
                ->where('levels_id', '=', Auth::user()->level_id)
                ->where('is_mini_app', '=', 0)
                ->get();
            $document = Document::findOrFail($id);
    
            $acts = DB::table('acts')
                ->select('acts.*')
                ->where('acts.document_id', '=', $id)
                ->get();
    
            $sub_categories = sub_category::where('category_id', $document->category_id)->get();
            $levels = Levels::all();
    
            $documents = DB::table('documents')
                ->select('documents.*')
                ->join('documents_levels', 'documents.id', '=', 'documents_levels.document_id')
                ->where('levels_id', '=', Auth::user()->level_id)
                ->get();
    
            $currentMenu = $document->category_id;
            return view('documents.act', compact('document', 'levels', 'categories', 'sub_categories', 'documents', 'acts', 'currentMenu'));
        } catch (\Throwable $th) {
        }
    }

    public function storeAct(Request $request) {
        try {
            $file = $request->file('file')->store('acts');
            $act = Act::create([
                'name' => $request['name'],
                'number' => $request['number'],
                'version' => $request['version'],
                'confirmedDate' => $request['confirmedDate'],
                'secret_level' => $request['secret_level'],
                'path' => $file,
                'document_id' => $request['document_id']
            ]);

            return redirect()->back()->with('success', 'Маягт амжилттай үүслээ.');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'Амжилтгүй: '.$th);
        }
    }

    public function updateAct(Request $request, $id)
    {
        $act = Act::find($id);
        $act->name = $request['name'];
        $act->version = $request['version'] ? $request['version'] : '';
        $act->confirmedDate = $request['confirmedDate'] ? $request['confirmedDate'] : '';
        $act->number = $request['number'] ? $request['number'] : '';
        $act->secret_level = $request['secret_level'];

        if($request->file) {
            Storage::delete($act->path);

            $filepath = $request->file('file')->store('acts');
            $act->path = $filepath;
        }

        $act->save();

        LogDocument::create([
            'comment' => $request['comment'] ? $request['comment'] : 'Холбогдох маягт засварлав',
            'document_id' => $request['document_id']
        ]);

        return redirect()->back()->with('success', 'Амжилттай засварлалаа.');
    }

    // Change file of document.
    public function changefileOfAct(Request $request, $id) {
        if($request->file) {
            $docu = Document::findOrFail($id);
            Storage::delete($docu->path);

            $filepath = $request->file('file')->store('documents');
            $docu->path = $filepath;
            $docu->save();
            return redirect()->back()->with('success', 'Амжилттай засварлалаа.');
        }else {
            return redirect()->back()->with('error', 'Амжилтгүй: Файл сонгоно уу!.');
        }
    }

    public function deleteAct($id) {
        $file = Act::findOrFail($id)->path;
        Storage::delete($file);

        Act::destroy($id);
        return redirect()->back()->with('success', 'Амжилттай устгалаа.');
    }

    public function downloadAct($id) {
        $file = Act::findOrFail($id)->path;
        return Storage::download($file);
    }
}

<?php

namespace App\Http\Controllers;

use App\CategoriesLevels;
use Illuminate\Http\Request;
use App\Category;
use App\sub_category;
use App\Levels;
use App\Departments;
use App\Jobs;
use App\Roles;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{
    public function index() {
        if(Auth::user()) {
            if(Auth::user()->role == 'admin') {
                $levels = Levels::all();
                $categories_all = Category::all();
                $categories = DB::table('categories')
                    ->select('categories.*')
                    ->join('categories_levels', 'categories.id', '=', 'categories_levels.category_id')
                    ->where('levels_id', '=', Auth::user()->level_id)
                    ->where('is_mini_app', '=', 0)
                    ->get();
                $sub_categories = sub_category::all();
                $departments = Departments::all();
                $jobs = Jobs::all();
                $roles = Roles::all();
                $currentMenu = 9999999998;
                // $categoriesLevels = CategoriesLevels::all();
                return view('settings.index', compact('categories', 'sub_categories', 'levels', 'departments','jobs', 'roles', 'currentMenu', 'categories_all'));    
            } else {
                return redirect('/');
            }
        } else {
            return redirect('/');
        }
    }
}

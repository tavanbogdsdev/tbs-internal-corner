<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\act;
use App\sub_category;
use App\Levels;
use App\CategoriesLevels;
use App\LogLogin;
use App\document as Documents;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $categories = DB::table('categories')
            ->select('categories.*')
            ->join('categories_levels', 'categories.id', '=', 'categories_levels.category_id')
            ->where('levels_id', '=', Auth::user()->level_id)
            ->where('is_mini_app', '=', 0)
            ->get();
        $acts = Act::all();
        $category = Category::findOrFail($id);
        $sub_categories = sub_category::where('category_id', $id)->get();
        $levels = Levels::all();

        $own = false;
        if(Auth::user()->role == 'moderator') {
            foreach(Auth::user()->categories as $cat) {
                if($cat->id == $id) {
                    $own = true;
                }
            }
        } else {
            $own = false;
        }

        if($own) {
            $documents = DB::table('documents')
            ->where('category_id', '=', $id)
            ->get();        
        } else {
            $documents = DB::table('documents')
            ->select('documents.*')
            ->join('documents_levels', 'documents.id', '=', 'documents_levels.document_id')
            ->where('levels_id', '=', Auth::user()->level_id)
            ->get();    
        }

        LogLogin::create([
            'comment' => $category->nickname . ' хуудас',
            'user_id' => Auth::user()->id
        ]);

        $currentMenu = $id;

        return view('documents.category', compact('levels', 'categories', 'sub_categories', 'category', 'documents', 'acts', 'currentMenu'));
    }

    public function showDocuments($catid, $id, $catname) {
        // return $catid;
        $categories = DB::table('categories')
            ->select('categories.*')
            ->join('categories_levels', 'categories.id', '=', 'categories_levels.category_id')
            ->where('levels_id', '=', Auth::user()->level_id)
            ->where('is_mini_app', '=', 0)
            ->get();

        $acts = Act::all();
        $category = Category::findOrFail($catid);
        $sub_categories = sub_category::where('category_id', $catid)->get();
        $levels = Levels::all();

        $own = false;
        if(Auth::user()->role == 'moderator') {
            foreach(Auth::user()->categories as $cat) {
                if($cat->id == $catid) {
                    $own = true;
                }
            }
        } else {
            $own = false;
        }

        if($own) {
            if($id == 0) {
                // $documents = DB::table('documents')
                // ->where('category_id', '=', $catid)
                // // ->join()
                // ->get();        
                $documents = Documents::all()->where('category_id', $catid);

            } else {
                // $documents = DB::table('documents')
                // ->where('sub_id', '=', $id)
                // ->get();      
                $documents = Documents::all()->where('sub_id', $id);

            }
        } else {
            if($id == 0) {
                $documents = DB::table('documents')
                ->select('documents.*')
                ->join('documents_levels', 'documents.id', '=', 'documents_levels.document_id')
                ->where('levels_id', '=', Auth::user()->level_id)
                ->where('category_id', '=', $catid)
                ->get();    
            } else {
                $documents = DB::table('documents')
                ->select('documents.*')
                ->join('documents_levels', 'documents.id', '=', 'documents_levels.document_id')
                ->where('levels_id', '=', Auth::user()->level_id)
                ->where('sub_id', '=', $id)
                ->get();    

                // $documents = Documents::find(1)->levels();
                    // ->join('documents_levels', 'documents.id', '=', 'documents_levels.document_id')
                    // ->where('levels_id', Auth::user()->level_id);
                    // ->where('category_id', '=', $catid);

            }

        }
        // return $documents;

        LogLogin::create([
            'comment' => $category->nickname . ' хуудас',
            'user_id' => Auth::user()->id
        ]);

        $currentMenu = $catid;
        $catname = $catname;


        return view('documents.index', compact('levels', 'categories', 'sub_categories', 'category', 'documents', 'acts', 'currentMenu', 'catname'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cat = Category::create([
            'name' => $request['name'],
            'nickname' => $request['nickname'],
            'is_mini_app' => $request['is_mini_app'],
            'is_nested' => $request['is_nested'],
            'is_time_order' => $request['is_time_order']
        ]);

        Category::find($cat->id)->levels()->attach($request->levels);

        return redirect('/settings')->with('success', 'Ангилал амжилттай нэмэгдлээ.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'nickname' => ['required', 'string', 'max:255'],
            'is_mini_app' => ['required'],
            'is_nested' => ['required'],
            'is_time_order' => ['required']

        ]);
        Category::whereId($id)->update($validateData);

        if($request->levels) {
            CategoriesLevels::where('category_id', $id)->delete();
            Category::find($id)->levels()->attach($request->levels);
        }
        return redirect('/settings')->with('success', 'Ангилал амжилттай засварлалаа.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat = Category::findOrFail($id);
        $cat->delete();

        CategoriesLevels::where('category_id', $id)->delete();
        return redirect('/settings')->with('success', 'Ангилал амжилттай устгалаа.');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\sub_category;

class SubCategoryController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        sub_category::create([
            'name' => $request['name'],
            'category_id' => $request['category_id']
        ]);
        return redirect('/settings')->with('success', 'Дэд ангилал амжилттай үүслээ.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'category_id' => ['required'],
        ]);

        sub_category::whereId($id)->update($validateData);
        return redirect('/settings')->with('success', 'Дэд ангилал амжилттай засварлалаа.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sc = sub_category::findOrFail($id);
        $sc->delete();
        return redirect('/settings')->with('success', 'Дэд ангилал амжилттай устгалаа.');
    }
}

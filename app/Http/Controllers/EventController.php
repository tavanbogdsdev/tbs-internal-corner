<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Calendar;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Event;
class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id, $eid, $catname)
    {
        $categories = DB::table('categories')
            ->select('categories.*')
            ->join('categories_levels', 'categories.id', '=', 'categories_levels.category_id')
            ->where('levels_id', '=', Auth::user()->level_id)
            ->where('is_mini_app', '=', 0)
            ->get();

        $events = [];
        if($eid == 0) {
            $data = DB::table('events')
            ->select('events.*', 'users.name as username', 'users.phone_number')
            ->where('events.category_id', '=', $id)
            ->join('users', 'users.id', '=', 'events.user_id')
            ->get();
        } else {
            $data = DB::table('events')
            ->select('events.*', 'users.name as username', 'users.phone_number')
            ->where('events.category_id', '=', $id)
            ->where('events.sub_id', '=', $eid)
            ->join('users', 'users.id', '=', 'events.user_id')
            ->get();
        }
        if($data->count()) {
            foreach ($data as $key => $value) {
                $events[] = Calendar::event(
                    $value->title . "\n" . $value->username ." " .date_format(new \DateTime($value->start_date),"H:i") . "-".date_format(new \DateTime($value->end_date),"H:i") ,
                    true,
                    new \DateTime($value->start_date),
                    new \DateTime($value->end_date),
                    $value->id,
                    // Add color and link on event
	                [
	                    'color' => '#c7eed8',   
	                ]
                );
            }
        }
        
        $own = false;
        if(Auth::user()->role == 'moderator') {
            foreach(Auth::user()->categories as $cat) {
                if($cat->id == $id) {
                    $own = true; //  Энэ ангилалын модератор мөн
                }
            }
        } else {
            $own = false;
        }

        if($own) {
            $calendar = Calendar::addEvents($events)->setOptions([
                'firstDay' => 1,
            ])
            ->setCallbacks([
                'eventClick' => 'function(event) { 
                    ModeratorModel.style.display = "block";
                    $("#btn_delete").val(event.id);
                    $("#ModeratorContent").text(event.title);
                 }',
            ]);    
        } else {
            $calendar = Calendar::addEvents($events)->setOptions([
                'firstDay' => 1,
            ])
            ->setCallbacks([
                'eventClick' => 'function(event) { 
                    Normalmodal.style.display = "block";
                    $("#modalContent").text(event.title);
                }',
            ]);    
        }

        $currentMenu = 0;
        return view('events.index', compact('calendar', 'currentMenu', 'categories', 'catname', 'id', 'eid'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $from = $request['start_date'] . ' ' . $request['start_hours'] . ':' . $request['start_minutes'];
        $to = $request['end_date'] . ' ' . $request['end_hours'] . ':' . $request['end_minutes'];


        $old = DB::table('events')
            ->where('sub_id', '=', $request['sub_id'])
            ->where('start_date', '<=', $from)
            ->where('end_date', '>', $from)
            ->get();

        if(count($old) != 0) {
            return redirect()->back()->with('error', ' Амжилтгүй: Тухайн цаг дээр захиалга өгөх боложгүй байна.');
        }


        $old1 = DB::table('events')
            ->where('sub_id', '=', $request['sub_id'])
            ->where('start_date', '<', $to)
            ->where('end_date', '>=', $to)
            ->get();

            if(count($old1) != 0) {
                return redirect()->back()->with('error', ' Амжилтгүй: Тухайн цаг дээр захиалга өгөх боложгүй байна.');
            }
    
        $old2 = DB::table('events')
            ->where('sub_id', '=', $request['sub_id'])
            ->where('start_date', '>=', $from)
            ->where('end_date', '<=', $to)
            ->get();

            if(count($old2) != 0) {
                return redirect()->back()->with('error', ' Амжилтгүй: Тухайн цаг дээр захиалга өгөх боложгүй байна.');
            }
        

        Event::create([
            'title' => $request['title'],
            'start_date' => $request['start_date'] . ' ' . $request['start_hours'] . ':' . $request['start_minutes'],
            'end_date' => $request['end_date'] . ' ' . $request['end_hours'] . ':' . $request['end_minutes'],
            'category_id' => $request['category_id'],
            'sub_id' => $request['sub_id'] == 0 ? null : $request['sub_id'],
            'user_id' => $request['user_id']
        ]);

        return redirect()->back()->with('success', ' Амжилттай үүслээ.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat = Event::findOrFail($id);
        $cat->delete();

        return redirect()->back()->with('success', 'Амжилттай устгалаа.');
    }
}

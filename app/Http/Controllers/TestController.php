<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\User;

class TestController extends Controller
{ public function testDB()
    {
        $users = DB::select("SELECT * FROM users");
        return response() -> json($users);
    }
    //testDB
}

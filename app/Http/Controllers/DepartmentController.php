<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Departments;

class DepartmentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            Departments::create([
                'name' => $request->name
            ]);
            return redirect('/settings')->with('success', 'Алба, нэгж амжилттай үүслээ.');
        } catch (\Throwable $th) {
            return redirect('/settings')->with('error', $th);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
        ]);

        Departments::whereId($id)->update($data);
        return redirect('/settings')->with('success', 'Алба, нэгж амжилттай хадгалагдлаа.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Departments::findOrFail($id);
        $item->delete();
        return redirect('/settings')->with('success', 'Алба, нэгж амжилттай устгалаа.');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Category;
use App\LogLogin;
use App\LogDocument;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
	    $role = Auth::user()->role;

        $documents = DB::table('documents')
            ->select('documents.*')
            ->join('documents_levels', 'documents.id', '=', 'documents_levels.document_id')
            ->where('levels_id', '=', Auth::user()->level_id)
            ->get();

        $categories = DB::table('categories')
            ->select('categories.*')
            ->join('categories_levels', 'categories.id', '=', 'categories_levels.category_id')
            ->where('levels_id', '=', Auth::user()->level_id)
            ->where('is_mini_app', '=', 0)
            ->get();

            
        $apps = DB::table('categories')
            ->select('categories.*')
            ->join('categories_levels', 'categories.id', '=', 'categories_levels.category_id')
            ->where('levels_id', '=', Auth::user()->level_id)
            ->where('is_mini_app', '=', 1)
            ->get();

        LogLogin::create([
            'comment' => 'Нүүр хуудас',
            'user_id' => Auth::user()->id
        ]);

        $log_logins = DB::table('log_logins')
            ->select('log_logins.*', 'users.name as name', 'users.lastname as lastname')
            ->join('users', 'users.id', '=', 'log_logins.user_id')
            ->orderBy('id', 'desc')
            ->get()
            ->take(35);

        $log_documents = DB::table('log_documents')
            ->select('log_documents.*', 'documents.name as name', 'documents.version as version')
            ->join('documents', 'documents.id', '=', 'log_documents.document_id')
            ->join('documents_levels', 'documents_levels.document_id', '=', 'log_documents.document_id')
            ->where('documents_levels.levels_id', '=', Auth::user()->level_id)
            ->orderBy('id', 'desc')
            ->get()
            ->take(35);

        $currentMenu = 0;

        return view('home', compact('role', 'categories', 'documents', 'log_logins', 'log_documents', 'currentMenu', 'apps'));
    }
}

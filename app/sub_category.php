<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;

class sub_category extends Model
{
    protected $fillable = [
        'name',
        'category_id'
    ];

    public function getCategoryAttribute() {
        return Category::find($this->category_id) ? Category::find($this->category_id)->nickname : ' ';
    }

    public function category() {
        return $this->belongsTo('App\Category', 'category_id', 'id');
    }
}

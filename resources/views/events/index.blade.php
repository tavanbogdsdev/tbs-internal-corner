@extends('layouts.app')

@section('style')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
<link rel="stylesheet" href="{{ asset('css/fullcalendar.min.css') }}" />
<script>
    var Normalmodal = document.getElementById("myModal");
    var ModeratorModel = document.getElementById("ModeratorModel_id");
    var span = document.getElementById("spanN");
    var spanM = document.getElementById("spanM");
    var btnN_Close = document.getElementById("btn_Close");
    var btnM_Close = document.getElementById("btnM_Close");
    var btn_delete = document.getElementById("btn_delete");
    btn_delete.onclick = function() {
        deleteFunction(this);
        ModeratorModel.style.display = "none";
    }
    btnM_Close.onclick = function() {
        ModeratorModel.style.display = "none";
    }

    btn_Close.onclick = function() {
        Normalmodal.style.display = "none";
        ModeratorModel.style.display = "none";
    }
    spanM.onclick = function() {
        Normalmodal.style.display = "none";
        ModeratorModel.style.display = "none";
    }
    span.onclick = function() {
        Normalmodal.style.display = "none";
        ModeratorModel.style.display = "none";
    }
    window.onclick = function(event) {
        if (event.target == ModeratorModel || event.target == Normalmodal) {
            Normalmodal.style.display = "none";
            ModeratorModel.style.display = "none";
        }
    }
</script>
<style>
    .fc-time-grid-container {
        height: 500px;
    }

    .btn.btn_Close {
        margin-left: 10px;
        color: white;
        width: 80px;
        background-color: #6c757d;
        border-color: #6c757d;
    }

    .NormalModal {
        display: none;
        padding-top: 10%;
        width: 100%;
        /* Full width */
        vertical-align: middle;
        height: 100%;
        /* Full height */
        overflow: auto;
        /* Enable scroll if needed */
        background-color: rgb(0, 0, 0);
        /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4);
        /* Black w/ opacity */
    }

    .header {
        position: absolute;
    }

    .ModeratorModal {
        display: none;
        padding-top: 10%;
        width: 100%;
        /* Full width */
        vertical-align: middle;
        height: 100%;
        /* Full height */
        overflow: auto;
        /* Enable scroll if needed */
        background-color: rgb(0, 0, 0);
        /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4);
        /* Black w/ opacity */
    }

    .btn.delete {
        color: white;
        width: 80px;
        background-color: #FF5733;
        border-color: #FF5733;
    }

    .content {
        display: flex;
        flex-direction: column;
        justify-content: center;
        width: 100%;
        height: 100%;
    }

    .Normal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 30%;
        height: 200px;
    }

    @media only screen and (max-width: 600px) {
        .Normal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 20px;
            border: 1px solid #888;
            width: 100%;
            height: 200px;
        }
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }
</style>
@endsection

@section('content')
<div class="container-fluid">
    <div class="card shadow-sm" style="border: 0">
        @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div><br />
        @endif
        @if(session()->get('error'))
        <div class="alert alert-danger">
            {{ session()->get('error') }}
        </div><br />
        @endif

        <div class="row">
            <div class="offset-1 col-9" style="padding: 1% 0 0 2%">
                <h5>{{ $catname }}</h5>
            </div>
            <div class="col-1">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#exampleModalCenter" style="margin-top: 10%">
                    <i class="fas fa-plus"></i>
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">{{ $catname }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form method="POST" action="{{ url('/event/create') }}">
                                @csrf
                                @method('POST')
                                <div class="modal-body">
                                    <input name="category_id" value="{{ $id }}" type="hidden" />
                                    <input name="sub_id" value="{{ $eid }}" type="hidden" />
                                    <input name="user_id" value="{{ Auth::user()->id }}" type="hidden" />

                                    <div class="form-group row">
                                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Зориулалт') }}</label>

                                        <div class="col-md-6" style="padding-right: 0;">
                                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="title" value="" required autocomplete="name" autofocus />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="start_date" class="col-md-4 col-form-label text-md-right">{{ __('Эхлэх өдөр') }}</label>

                                        <div class="col-md-3">
                                            <input id="start_date" type="date" class="form-control @error('start_date') is-invalid @enderror" name="start_date" data-format="dd/MM/yyyy hh:mm:ss" value="" required />
                                        </div>
                                        <label for="start_date" class="col-md-1 col-form-label text-md-right">{{ __('Цаг') }}</label>

                                        <div class="col-md-1" style="padding: 0;">
                                            <select id="start_hours" class="custom-select" name="start_hours">
                                                @for($i=8; $i<=19; $i++) <option value="{{ $i }}">{{ $i }}</option>
                                                    @endfor
                                            </select>
                                        </div>
                                        <div class="col-md-1" style="padding: 0;">
                                            <select id="start_minutes" class="custom-select" name="start_minutes">
                                                <option value="0">0</option>
                                                <option value="15">15</option>
                                                <option value="30">30</option>
                                                <option value="45">45</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="end_date" class="col-md-4 col-form-label text-md-right">{{ __('Дуусах өдөр') }}</label>

                                        <div class="col-md-3">
                                            <input id="end_date" type="date" class="form-control @error('end_date') is-invalid @enderror" name="end_date" value="" required />
                                        </div>
                                        <label for="start_date" class="col-md-1 col-form-label text-md-right">{{ __('Цаг') }}</label>

                                        <div class="col-md-1" style="padding: 0;">
                                            <select id="end_hours" class="custom-select" name="end_hours">
                                                @for($i=8; $i<=19; $i++) <option value="{{ $i }}">{{ $i }}</option>
                                                    @endfor
                                            </select>
                                        </div>
                                        <div class="col-md-1" style="padding: 0;">
                                            <select id="end_minutes" class="custom-select" name="end_minutes">
                                                <option value="0">0</option>
                                                <option value="15">15</option>
                                                <option value="30">30</option>
                                                <option value="45">45</option>
                                            </select>
                                        </div>

                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                    <button type="submit" class="btn btn-primary">Хадгалах</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <!-- Normal Modal -->
                <div id="myModal" class="modal NormalModal">
                    <div class="Normal-content modal-content">
                        <div>
                            @if($catname== 'Хурлын өрөөний цаг захиалга')
                            <h3 class="header">Хурлын өрөөний цаг захиалга</h3>
                            @else
                            <h3 class="header">Үйлчилгээний машины цаг захиалга</h3>
                            @endif

                            <span id="spanN" class="close">&times;</span>
                        </div>
                        <div id="modalContent" class="content"></div>
                        <div style="display: flex; flex-direction: row; align-self: flex-end;">
                            <button class="btn btn_Close" id="btn_Close">Close</button>
                        </div>
                    </div>
                </div>
                <!-- Moderator Modal -->
                <div id="ModeratorModel_id" class="modal ModeratorModal">
                    <div class="Normal-content modal-content">
                        <div>
                            @if($catname== 'Хурлын өрөөний цаг захиалга')
                            <h3 class="header">Хурлын өрөөний цаг захиалга</h3>
                            @else
                            <h3 class="header">Үйлчилгээний машины цаг захиалга</h3>
                            @endif
                            <span id="spanM" class="close">&times;</span>
                        </div>
                        <div id="ModeratorContent" class="content"></div>
                        <div style="display: flex; flex-direction: row; align-self: flex-end;">
                            <button class="btn delete" id="btn_delete" value="hello ?">Устах</button>
                            <button class="btn btn_Close" id="btnM_Close">Close</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-12 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">
                <div class="panel panel-default">
                    <div class="panel-body" id='calendar'>
                        {!! $calendar->calendar() !!}
                        {!! $calendar->script() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function deleteFunction(e) {
        window.location = "/event/delete/" + e.value;
    }
</script>

@endsection
@extends('layouts.app')

@section('content')
<div class="container-fluid">
    @if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div><br />
    @endif
    @if(session()->get('error'))
    <div class="alert alert-danger">
        {{ session()->get('error') }}
    </div><br />
    @endif

    <div class="row">
        <div class="col-lg-6">
            <div class="row">
                @foreach($apps as $app)
                <div class="col-12 col-sm-6" style="padding: 1%">
                    @if($app->is_nested)
                    <a href="{{ url('/category/'.$app->id) }}">
                        <div class="card subCard shadow-sm">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-8 subCardName">
                                        <span>{{ $app->name }}</span>
                                    </div>
                                    <div class="col-4">
                                        @if($app->name == 'Санхүү бүртгэлийн алба')
                                        <img src='{{ asset("img/finance.png") }}' style="width: 70%; margin-top: 15%;">
                                        @elseif($app->name == 'Компанийн болон брэнд танилцуулга')
                                        <img src='{{ asset("img/brand.png") }}' style="width: 70%; margin-top: 15%;">
                                        @elseif($app->name == 'Хурлын өрөөний цаг захиалга')
                                        <img src='{{ asset("img/meeting.png") }}' style="width: 70%; margin-top: 15%;">
                                        @elseif($app->name == 'Автомашин цаг захиалга')
                                        <img src='{{ asset("img/car.png") }}' style="width: 70%; margin-top: 15%;">
                                        @else
                                        <img src='{{ asset("img/logo_mini.png") }}' style="width: 70%; margin-top: 15%;">
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    @else
                    @if($app->is_time_order)
                    <a href="{{ url('/category/'.$app->id.'/events/0/'.$app->name) }}">
                        <div class="card subCard shadow-sm">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-8 subCardName">
                                        <span>{{ $app->name }}</span>
                                    </div>
                                    <div class="col-4">
                                        @if($app->name == 'Санхүүгийн анхан шатны баримтууд')
                                        <img src='{{ asset("img/finance.png") }}' style="width: 70%; margin-top: 15%;">
                                        @elseif($app->name == 'Компанийн болон брэнд танилцуулга')
                                        <img src='{{ asset("img/brand.png") }}' style="width: 70%; margin-top: 15%;">
                                        @elseif($app->name == 'Хурлын өрөөний цаг захиалга')
                                        <img src='{{ asset("img/meeting.png") }}' style="width: 70%; margin-top: 15%;">
                                        @elseif($app->name == 'Автомашин цаг захиалга')
                                        <img src='{{ asset("img/car.png") }}' style="width: 70%; margin-top: 15%;">
                                        @else
                                        <img src='{{ asset("img/logo_mini.png") }}' style="width: 70%; margin-top: 15%;">
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    @else
                    <a href="{{ url('/category/'.$app->id.'/documents/0/'.$app->name) }}">
                        <div class="card subCard shadow-sm">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-8 subCardName">
                                        <span>{{ $app->name }}</span>
                                    </div>
                                    <div class="col-4">
                                        @if($app->name == 'Санхүүгийн анхан шатны баримтууд')
                                        <img src='{{ asset("img/finance.png") }}' style="width: 70%; margin-top: 15%;">
                                        @elseif($app->name == 'Компанийн болон брэнд танилцуулга')
                                        <img src='{{ asset("img/brand.png") }}' style="width: 70%; margin-top: 15%;">
                                        @elseif($app->name == 'Хурлын өрөөний цаг захиалга')
                                        <img src='{{ asset("img/meeting.png") }}' style="width: 70%; margin-top: 15%;">
                                        @elseif($app->name == 'Автомашин цаг захиалга')
                                        <img src='{{ asset("img/car.png") }}' style="width: 70%; margin-top: 15%;">
                                        @else
                                        <img src='{{ asset("img/logo_mini.png") }}' style="width: 70%; margin-top: 15%;">
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    @endif
                    @endif
                </div>
                @endforeach
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card infoPad shadow-sm">
                <div class="row">
                    <div class="col-md-6 context">
                        <strong>
                            Алсын хараа:
                        </strong><br />
                        <span>
                            Технологийн шийдлийг
                            <br />
                            бизнес бүрд
                        </span>

                    </div>
                    <div class="col-md-6 conimg_right" style="text-align: center;">
                        <img src='{{ asset("img/vision.png") }}' style="width: 50%;">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 conimg_left" style="text-align: center;">
                        <img src='{{ asset("img/mission.png") }}' style="width: 50%; "\>
                    </div>
                    <div class="col-md-6 context">
                        <strong>
                            Эрхэм зорилго:
                        </strong><br />
                        <span>
                            Бид технологийн цогц шийдэл,<br />
                            хүлээлтээс давсан үйлчилгээгээр<br />
                            нийгмийг дижитал<br />
                            хувьсалд дагуулна.

                        </span>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 context">
                        <strong>
                            Үнэт зүйлс:
                        </strong><br />
                        <span>
                            Бүтээлч сэтгэлгээ<br />
                            Багийн ажиллагаа<br />
                            Гал эрмэлзэл<br />
                            Эерэг хандлага<br />
                            Гүйцэтгэлийн төгөлдөршил

                        </span>

                    </div>
                    <div class="col-md-6 conimg_right" style="text-align: center;">
                        <img src='{{ asset("img/value.png") }}' style="width: 50%;">
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
@section('style')
<style>
    .loginRow:hover {
        background-color: silver
    }

    .subCardName {
        padding-top: 5%;
        padding-left: 5%;
        font-size: 1rem;
        color: #5A607F
    }

    .subCard {
        border: 0;
        border-radius: 7px;
        min-height: 150px;
        margin: 0 4% 4% 4%;
    }

    .subCard:hover {
        background-color: #F2F7FF;
        cursor: pointer;
    }

    .infoPad {
        border: 0;
        border-radius: 7px;
        margin-right: 2%;
        min-height: 200px;
        margin-top: 1%;
    }
    .context{
        padding: 10%;
        text-align: center;
        font-size: 1rem;
        color: #5A607F;
    }
    .conimg_left {
        padding-top: 5%;
        /* padding: 5% 0 5% 13%; */
    }
    .conimg_right {
        padding-top: 5%;
        /* padding-top: 5%;
        padding-left: 6%; */
    }
</style>
@endsection
@extends('layouts.app')

@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<div class="container-fluid">
    <div class="card" style="border: 0">
        <div class="card-body">
            @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div><br />
            @endif
            @if(session()->get('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div><br />
            @endif
            <div class="row">
                <div class="col-md-12">
                    <h5>Тохиргоо</h5>
                </div>
            </div>
            <br>

            <div class="row">
                <div class="col-lg-3">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link active" id="v-pills-category-tab" data-toggle="pill" href="#v-pills-category" role="tab" aria-controls="v-pills-category" aria-selected="true">Ангилал
                            <button class="btn btn-info btn-sm float-right" data-toggle="modal" data-target="#categoryAddModal">+</button>
                        </a>
                        <a class="nav-link" id="v-pills-sub_category-tab" data-toggle="pill" href="#v-pills-sub_category" role="tab" aria-controls="v-pills-sub_category" aria-selected="false">Дэд ангилал
                            <button class="btn btn-info btn-sm float-right" data-toggle="modal" data-target="#subCategoryAddModal">+</button>
                        </a>
                        <a class="nav-link" id="v-pills-level-tab" data-toggle="pill" href="#v-pills-level" role="tab" aria-controls="v-pills-level" aria-selected="false">Түвшин
                            <button class="btn btn-info btn-sm float-right" data-toggle="modal" data-target="#levelAddModal">+</button>
                        </a>
                        <a class="nav-link" id="v-pills-role-tab" data-toggle="pill" href="#v-pills-role" role="tab" aria-controls="v-pills-role" aria-selected="false">Хандах эрх</a>
                        <a class="nav-link" id="v-pills-department-tab" data-toggle="pill" href="#v-pills-department" role="tab" aria-controls="v-pills-department" aria-selected="false">Алба, нэгж
                            <button class="btn btn-info btn-sm float-right" data-toggle="modal" data-target="#departmentAddModal">+</button>
                        </a>
                        <a class="nav-link" id="v-pills-job-tab" data-toggle="pill" href="#v-pills-job" role="tab" aria-controls="v-pills-job" aria-selected="false">Албан тушаал
                            <button class="btn btn-info btn-sm float-right" data-toggle="modal" data-target="#jobAddModal">+</button>
                        </a>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-category" role="tabpanel" aria-labelledby="v-pills-category-tab">
                            <!-- Modal Add category -->
                            <div class="modal fade" id="categoryAddModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Ангилал нэмэх</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form method="POST" action="{{ route('category.store') }}">
                                            <div class="modal-body">
                                                @csrf

                                                <div class="form-group row">
                                                    <label for="nickname" class="col-md-4 col-form-label text-md-right">{{ __('Товч нэр') }}</label>

                                                    <div class="col-md-6">
                                                        <input id="nickname" type="text" class="form-control @error('nickname') is-invalid @enderror" name="nickname" value="{{ old('nickname') }}" required autocomplete="name" autofocus>

                                                        @error('nickname')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Нэр') }}</label>

                                                    <div class="col-md-6">
                                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                                        @error('name')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="levels" class="col-md-4 col-form-label text-md-right">{{ __('Мини апп уу?') }}</label>

                                                    <div class="col-md-6">
                                                        <select id="levels" class="custom-select" name="is_mini_app">
                                                            <option value="0">Үгүй</option>
                                                            <option value="1">Тийм</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="levels" class="col-md-4 col-form-label text-md-right">{{ __('Дэд цэстэй юу?') }}</label>

                                                    <div class="col-md-6">
                                                        <select id="levels" class="custom-select" name="is_nested">
                                                            <option value="0">Үгүй</option>
                                                            <option value="1">Тийм</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="levels" class="col-md-4 col-form-label text-md-right">{{ __('Цаг захиалгатай юу?') }}</label>

                                                    <div class="col-md-6">
                                                        <select id="levels" class="custom-select" name="is_time_order">
                                                            <option value="0">Үгүй</option>
                                                            <option value="1">Тийм</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="levels" class="col-md-4 col-form-label text-md-right">{{ __('Хандах түвшин') }}</label>

                                                    <div class="col-md-6">
                                                        <select id="levels" class="custom-select" multiple="multiple" name="levels[]">
                                                            @foreach($levels as $level)
                                                            <option value="{{ $level->id }}">{{ $level->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                                <button type="submit" class="btn btn-primary">Нэмэх</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <table class="table table-sm table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th scope="col">Товч нэр</th>
                                        <th scope="col">Нэр</th>
                                        <th scope="col">Мини апп</th>
                                        <th scope="col">Дэд цэстэй</th>
                                        <th scope="col">Цаг захиалга</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php( $cat_count = 1 )
                                    @foreach($categories_all as $category)
                                    <tr>
                                        <td>{{ $cat_count++ }}</td>
                                        <td>{{ $category->nickname }}</td>
                                        <td>{{ $category->name }}</td>
                                        <td>{{ $category->is_mini_app == 0 ? 'Үгүй' : 'Тийм' }}</td>
                                        <td>{{ $category->is_nested == 0 ? 'Үгүй' : 'Тийм' }}</td>
                                        <td>{{ $category->is_time_order == 0 ? 'Үгүй' : 'Тийм' }}</td>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#editCategoryModal{{ $category->id }}">
                                                <i class="fas fa-pen-fancy"></i>
                                            </a>
                                            <!-- Modal -->
                                            <div class="modal fade" id="editCategoryModal{{ $category->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Засах</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form method="POST" action="{{ route('category.update', $category->id) }}">
                                                            <div class="modal-body">
                                                                @csrf
                                                                @method('PATCH')

                                                                <div class="form-group row">
                                                                    <label for="nickname" class="col-md-4 col-form-label text-md-right">{{ __('Товч нэр') }}</label>

                                                                    <div class="col-md-6">
                                                                        <input id="nickname" type="text" class="form-control @error('nickname') is-invalid @enderror" name="nickname" value="{{ $category->nickname }}" required autocomplete="name" autofocus>

                                                                        @error('nickname')
                                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $message }}</strong>
                                                                        </span>
                                                                        @enderror
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Нэр') }}</label>

                                                                    <div class="col-md-6">
                                                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $category->name }}" required autocomplete="name" autofocus>

                                                                        @error('name')
                                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $message }}</strong>
                                                                        </span>
                                                                        @enderror
                                                                    </div>
                                                                </div>


                                                                <div class="form-group row">
                                                                    <label for="levels" class="col-md-4 col-form-label text-md-right">{{ __('Мини апп уу?') }}</label>

                                                                    <div class="col-md-6">
                                                                        <select id="levels" class="custom-select" name="is_mini_app">
                                                                            @if($category->is_mini_app)
                                                                            <option value="0">Үгүй</option>
                                                                            <option value="1" selected>Тийм</option>
                                                                            @else
                                                                            <option value="0" selected>Үгүй</option>
                                                                            <option value="1">Тийм</option>
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label for="levels" class="col-md-4 col-form-label text-md-right">{{ __('Дэд цэстэй юу?') }}</label>

                                                                    <div class="col-md-6">
                                                                        <select id="levels" class="custom-select" name="is_nested">
                                                                            @if($category->is_nested)
                                                                            <option value="0">Үгүй</option>
                                                                            <option value="1" selected>Тийм</option>
                                                                            @else
                                                                            <option value="0" selected>Үгүй</option>
                                                                            <option value="1">Тийм</option>
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label for="levels" class="col-md-4 col-form-label text-md-right">{{ __('Цаг захиалгатай юу?') }}</label>

                                                                    <div class="col-md-6">
                                                                        <select id="levels" class="custom-select" name="is_time_order">
                                                                            @if($category->is_time_order)
                                                                            <option value="0">Үгүй</option>
                                                                            <option value="1" selected>Тийм</option>
                                                                            @else
                                                                            <option value="0" selected>Үгүй</option>
                                                                            <option value="1">Тийм</option>
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label for="levels" class="col-md-4 col-form-label text-md-right">{{ __('Хандах түвшин') }}</label>
                                                                    <?php
                                                                        $c_level = [];
                                                                        foreach($levels as $level) {
                                                                            foreach($category->levels as $c){
                                                                                if($level->id === $c->id) {
                                                                                    $level->isSelect = true;
                                                                                    break;
                                                                                } else {
                                                                                    $level->isSelect = false;
                                                                                }
                                                                            }
                                                                            array_push($c_level, $level);
                                                                        }
                                                                    ?>

                                                                    <div class="col-md-6">
                                                                        <select id="levels" class="multiSelectLevels" multiple name="levels[]" required>
                                                                            @foreach($c_level as $cccc)
                                                                            @if($cccc->isSelect)
                                                                            <option value="{{ $cccc->id }}"  selected="selected">{{ $cccc->name }}</option>
                                                                            @else
                                                                            <option value="{{ $cccc->id }}">{{ $cccc->name }}</option>
                                                                            @endif
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                                                <button type="submit" class="btn btn-primary">Засах</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            <a href="#" data-toggle="modal" data-target="#deleteCategoryModal{{ $category->id }}" style='color: #FF5412'>
                                                <i class="fas fa-trash-alt"></i>
                                            </a>
                                            <!-- Modal -->
                                            <div class="modal fade" id="deleteCategoryModal{{ $category->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog  modal-sm">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Устгах</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form method="post" action="{{ route('category.destroy', $category->id) }}">
                                                            @csrf
                                                            @method('DELETE')

                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                                                <button type="submit" class="btn btn-primary">Устгах</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="v-pills-sub_category" role="tabpanel" aria-labelledby="v-pills-sub_category-tab">
                            <!-- Modal Add sub category -->
                            <div class="modal fade" id="subCategoryAddModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Дэд ангилал нэмэх</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form method="POST" action="{{ route('sub_category.store') }}">
                                            <div class="modal-body">
                                                @csrf

                                                <div class="form-group row">
                                                    <label for="category" class="col-md-4 col-form-label text-md-right">{{ __('Ангилал') }}</label>

                                                    <div class="col-md-6">
                                                        <select id="category" class="form-control @error('category') is-invalid @enderror" name="category_id" required autofocus>
                                                            @foreach($categories_all as $item)
                                                            <option value={{ $item->id }}>{{ $item->nickname}}</option>
                                                            @endforeach
                                                        </select>

                                                        @error('category')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Нэр') }}</label>

                                                    <div class="col-md-6">
                                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                                        @error('name')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                                <button type="submit" class="btn btn-primary">Нэмэх</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <table class="table table-sm table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th scope="col">Ангилал</th>
                                        <th scope="col">Нэр</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php( $sub_count = 1 )
                                    @foreach($sub_categories as $item)
                                    <tr>
                                        <td>{{ $sub_count++ }}</td>
                                        <td>{{ $item->category }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#editSubCategoryModal{{ $item->id }}">
                                                <i class="fas fa-pen-fancy"></i>
                                            </a>
                                            <!-- Modal -->
                                            <div class="modal fade" id="editSubCategoryModal{{ $item->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Засах</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form method="POST" action="{{ route('sub_category.update', $item->id) }}">
                                                            <div class="modal-body">
                                                                @csrf
                                                                @method('PATCH')

                                                                <div class="form-group row">
                                                                    <label for="category" class="col-md-4 col-form-label text-md-right">{{ __('Товч нэр') }}</label>

                                                                    <div class="col-md-6">
                                                                        <select id="category" class="form-control @error('category') is-invalid @enderror" name="category_id" required autofocus>
                                                                            @foreach($categories_all as $cat)
                                                                            @if($cat->nickname == $item->category)
                                                                            <option value="{{ $cat->id }}" selected>{{ $cat->nickname }}</option>
                                                                            @else
                                                                            <option value="{{ $cat->id }}">{{ $cat->nickname }}</option>
                                                                            @endif
                                                                            @endforeach
                                                                        </select>

                                                                        @error('category')
                                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $message }}</strong>
                                                                        </span>
                                                                        @enderror
                                                                    </div>
                                                                </div>

                                                                <div class="form-group row">
                                                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Нэр') }}</label>

                                                                    <div class="col-md-6">
                                                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $item->name }}" required autocomplete="name" autofocus>

                                                                        @error('name')
                                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $message }}</strong>
                                                                        </span>
                                                                        @enderror
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                                                <button type="submit" class="btn btn-primary">Засах</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            <a href="#" data-toggle="modal" data-target="#deleteSubCategoryModal{{ $item->id }}" style='color: #FF5412'>
                                                <i class="fas fa-trash-alt"></i>
                                            </a>
                                            <!-- Modal -->
                                            <div class="modal fade" id="deleteSubCategoryModal{{ $item->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog  modal-sm">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Устгах</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form method="POST" action="{{ route('sub_category.destroy', $item->id ) }}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                                                <button type="submit" class="btn btn-primary">Устгах</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="v-pills-level" role="tabpanel" aria-labelledby="v-pills-level-tab">
                            <!-- Modal Add level -->
                            <div class="modal fade" id="levelAddModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Түвшин нэмэх</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form method="POST" action="{{ route('level.store') }}">
                                            <div class="modal-body">
                                                @csrf
                                                <div class="form-group row">
                                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Нэр') }}</label>

                                                    <div class="col-md-6">
                                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                                        @error('name')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                                <button type="submit" class="btn btn-primary">Нэмэх</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <table class="table table-sm table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th scope="col">Нэр</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php($level_count = 1)
                                    @foreach($levels as $level)
                                    <tr>
                                        <td>{{ $level_count++ }}</td>
                                        <td>{{ $level->name }}</td>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#editLevelModal{{ $level->id }}">
                                                <i class="fas fa-pen-fancy"></i>
                                            </a>
                                            <!-- Modal -->
                                            <div class="modal fade" id="editLevelModal{{ $level->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Засах</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form method="POST" action="{{ route('level.update', $level->id ) }}">
                                                            <div class="modal-body">
                                                                @csrf
                                                                @method('PATCH')
                                                                <div class="form-group row">
                                                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Нэр') }}</label>

                                                                    <div class="col-md-6">
                                                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $level->name }}" required autocomplete="name" autofocus>

                                                                        @error('name')
                                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $message }}</strong>
                                                                        </span>
                                                                        @enderror
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                                                <button type="submit" class="btn btn-primary">Засах</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            <a href="#" data-toggle="modal" data-target="#deleteLevelModal{{ $level->id }}" style='color: #FF5412'>
                                                <i class="fas fa-trash-alt"></i>
                                            </a>
                                            <!-- Modal -->
                                            <div class="modal fade" id="deleteLevelModal{{ $level->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog  modal-sm">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Устгах</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form method="POST" action="{{ route('level.destroy', $level->id ) }}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                                                <button type="submit" class="btn btn-primary">Устгах</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="v-pills-role" role="tabpanel" aria-labelledby="v-pills-role-tab">
                            <table class="table table-sm table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th scope="col">Нэр</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php($role_count=1)
                                    @foreach($roles as $role)
                                    <tr>
                                        <td>{{ $role_count++ }}</td>
                                        <td>{{ $role->name }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                        <div class="tab-pane fade" id="v-pills-department" role="tabpanel" aria-labelledby="v-pills-department-tab">
                            <div class="modal fade" id="departmentAddModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Алба нэгж нэмэх</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form method="POST" action="{{ route('department.store') }}">
                                            <div class="modal-body">
                                                @csrf
                                                <div class="form-group row">
                                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Нэр') }}</label>

                                                    <div class="col-md-6">
                                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                                        @error('name')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                                <button type="submit" class="btn btn-primary">Нэмэх</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <table class="table table-sm table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th scope="col">Нэр</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php($dep_count = 1)
                                    @foreach($departments as $item)
                                    <tr>
                                        <td>{{ $dep_count++ }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#editDepartmentModal{{ $item->id }}">
                                                <i class="fas fa-pen-fancy"></i>
                                            </a>
                                            <!-- Modal -->
                                            <div class="modal fade" id="editDepartmentModal{{ $item->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Засах</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form method="POST" action="{{ route('department.update', $item->id) }}">
                                                            <div class="modal-body">
                                                                @csrf
                                                                @method('PATCH')
                                                                <div class="form-group row">
                                                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Нэр') }}</label>

                                                                    <div class="col-md-6">
                                                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $item->name }}" required autocomplete="name" autofocus>

                                                                        @error('name')
                                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $message }}</strong>
                                                                        </span>
                                                                        @enderror
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                                                <button type="submit" class="btn btn-primary">Засах</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            <a href="#" data-toggle="modal" data-target="#deleteDepartmentModal{{ $item->id }}" style='color: #FF5412'>
                                                <i class="fas fa-trash-alt"></i>
                                            </a>
                                            <!-- Modal -->
                                            <div class="modal fade" id="deleteDepartmentModal{{ $item->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog  modal-sm">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Устгах</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form method="POST" action="{{ route('department.destroy', $item->id) }}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                                                <button type="submit" class="btn btn-primary">Устгах</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="v-pills-job" role="tabpanel" aria-labelledby="v-pills-job-tab">
                            <div class="modal fade" id="jobAddModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Албан тушаал нэмэх</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form method="POST" action="{{ route('job.store') }}">
                                            <div class="modal-body">
                                                @csrf
                                                <div class="form-group row">
                                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Нэр') }}</label>

                                                    <div class="col-md-6">
                                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                                        @error('name')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                                <button type="submit" class="btn btn-primary">Нэмэх</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <table class="table table-sm table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th scope="col">Нэр</th>
                                        <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php($job_count = 1)
                                    @foreach($jobs as $job)
                                    <tr>
                                        <td>{{ $job_count++ }}</td>
                                        <td>{{ $job->name }}</td>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#editJobModal{{ $job->id }}">
                                                <i class="fas fa-pen-fancy"></i>
                                            </a>
                                            <!-- Modal -->
                                            <div class="modal fade" id="editJobModal{{ $job->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Засах</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form method="POST" action="{{ route('job.update', $job->id ) }}">
                                                            <div class="modal-body">
                                                                @csrf
                                                                @method('PATCH')
                                                                <div class="form-group row">
                                                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Нэр') }}</label>

                                                                    <div class="col-md-6">
                                                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $job->name }}" required autocomplete="name" autofocus>

                                                                        @error('name')
                                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $message }}</strong>
                                                                        </span>
                                                                        @enderror
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                                                <button type="submit" class="btn btn-primary">Засах</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            <a href="#" data-toggle="modal" data-target="#deleteJobModal{{ $job->id }}" style='color: #FF5412'>
                                                <i class="fas fa-trash-alt"></i>
                                            </a>
                                            <!-- Modal -->
                                            <div class="modal fade" id="deleteJobModal{{ $job->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog  modal-sm">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">Устгах</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form method="POST" action="{{ route('job.destroy', $job->id) }}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                                                <button type="submit" class="btn btn-primary">Устгах</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
    $('.multiSelectLevels').select2();
    });
</script>
@endsection
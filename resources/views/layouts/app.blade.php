<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>IC 2.9</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.0.943/pdf.min.js"></script>
    <!-- Icon -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/simple-sidebar.css') }}" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <!-- <script src="/vendor/bootstrap/js/bootstrap.min.js"></script> -->

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <!-- <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
    <script src="{{ asset('js/select2.min.js') }}"></script> -->
    <script language="JavaScript">
        document.addEventListener("contextmenu", function(e) {
            e.preventDefault();
        }, false);
    </script>
</head>

<body>
    <div class="d-flex" id="wrapper">
        @auth
        <!-- Sidebar -->
        <div class="shadow-sm" id="sidebar-wrapper" style="background-color: white">
            <div class="sidebar-heading">
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src='{{ asset("img/logo.png") }}' style="background-color: white; width: 190px">
                </a>
            </div>
            <div class="list-group list-group-flush">
                @if($currentMenu == 0)
                <a class="list-group-item list-group-item-action activeMenu" href="{{ url('/') }}"> <i class="fa fa-home" aria-hidden="true" style="color: #0058FF"></i> Нүүр</a>
                @else
                <a class="list-group-item list-group-item-action" href="{{ url('/') }}"> <i class="fa fa-home" aria-hidden="true" style="color: #7E84A3"></i> Нүүр</a>
                @endif
                @foreach($categories as $item)
                @if($item->id == $currentMenu)
                <a class="list-group-item list-group-item-action activeMenu" active href="{{ url('/category/'.$item->id) }}">
                    @else
                    <a class="list-group-item list-group-item-action" href="{{ url('/category/'.$item->id) }}">
                        @endif
                        @if($item->nickname == 'МАБУТ')
                        @if($item->id == $currentMenu)
                        <i class="fas fa-shield-alt" style="color: #0058FF"></i>
                        @else
                        <i class="fas fa-shield-alt" style="color: #7E84A3"></i>
                        @endif
                        @endif

                        @if($item->nickname == 'ХЭМАБ')
                        @if($item->id == $currentMenu)
                        <i class="fas fa-hard-hat" style="color: #0058FF"></i>
                        @else
                        <i class="fas fa-hard-hat" style="color: #7E84A3"></i>
                        @endif
                        @endif

                        @if($item->nickname == 'Мэдээлэл технологи')
                        @if($item->id == $currentMenu)
                        <i class="fas fa-network-wired" style="color: #0058FF"></i>
                        @else
                        <i class="fas fa-network-wired" style="color: #7E84A3"></i>
                        @endif
                        @endif

                        @if($item->nickname == 'Механик')
                        @if($item->id == $currentMenu)
                        <i class="fas fa-car-side" style="color: #0058FF"></i>
                        @else
                        <i class="fas fa-car-side" style="color: #7E84A3"></i>
                        @endif
                        @endif
                        {{ $item->nickname }}
                    </a>
                    @endforeach
            </div>
        </div>
        <!-- /#sidebar-wrapper -->


        <!-- Page Content -->
        <div id="page-content-wrapper">

            <nav class="navbar navbar-expand-lg navbar-light" style="background-color: white; box-shadow: 0 .125rem 0 rgba(0,0,0,.075)!important">

                <div class="row">
                    <div class="col-md-1 col-sm-1 col-2">
                        <button class="btn btn-primary" id="menu-toggle"><i class="fa fa-bars" aria-hidden="true"></i>
                        </button>
                    </div>
                    <div class="col-md-9 col-sm-9 col-7">
                        <h6 style=" padding-top: 1%; color: #5A607F">ТАВАН БОГД СОЛЮШНС ХХК-ИЙН НИЙТ АЖИЛЧДЫН ДОТООД БУЛАН</h6>
                    </div>
                    <div class="col-md-1 col-sm-1 col-2">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </div>
                </div>



                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                        @if(Auth::user()->role === 'admin')
                        <li class="nav-item">
                            @if($currentMenu == 9999999999)
                            <a class="nav-link active" href="{{ route('users.index') }}">{{ __('Хэрэглэгч') }}</a>
                            @else
                            <a class="nav-link" href="{{ route('users.index') }}">{{ __('Хэрэглэгч') }}</a>
                            @endif
                        </li>
                        <li class="nav-item">
                            @if($currentMenu == 9999999998)
                            <a class="nav-link active" href="{{ route('settings') }}">{{ __('Тохиргоо') }}</a>
                            @else
                            <a class="nav-link" href="{{ route('settings') }}">{{ __('Тохиргоо') }}</a>
                            @endif
                        </li>
                        @endif
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('users.create') }}">{{ __('Цаг бүртгэл') }}</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Гарах') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>

                                <a class="dropdown-item" href="{{ url('/user/password/change') }}">
                                    {{ __('Нууц үг солих') }}
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="container-fluid">
                <main class="py-4">
                    @yield('content')
                </main>
            </div>
        </div>
        <!-- /#page-content-wrapper -->
        @endauth
        @guest
        <main class="py-4">
            @yield('content')
        </main>
        @endguest
    </div>
</body>
<style>
    .activeMenu {
        background-color: #F2F7FF;
        border-left: 5px solid #0058FF;
    }
</style>
@auth
<footer class="footer mt-auto py-3 navbar bottom">
    <div class="container">
        <span class="text-muted">Copyright © 2021 Бизнес хөгжүүлэлтийн алба. Таван Богд Солюшнс ХХК </span>
        <span class="text-muted"> <i class="fa fa-code-fork" aria-hidden="true"></i> Version2.1 </span>
    </div>
</footer>
@endauth
@yield('style')
@yield('scripts')
<!-- Menu Toggle Script -->
<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>

</html>
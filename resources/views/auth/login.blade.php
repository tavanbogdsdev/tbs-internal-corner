@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-8 mobile">
            <div class="card" style="border: 0; background-color: #f8fafc; padding: 22% 0 0 10%; text-align: center">
                <div class="card-header" style="background-color: #f8fafc; border: 0; padding-left: 20%; padding-right: 20%">
                    <h4><strong>ТАВАН БОГД СОЛЮШНС ХХК-ИЙН НИЙТ АЖИЛЧДЫН ДОТООД БУЛАНД ТАВТАЙ МОРИЛНО УУ.</strong></h4>
                </div>
                <div class="card-body" style="background-color: #f8fafc">
                    <img src='{{ asset("img/home.png") }}' style="background-color: #f8fafc; width: 55%">
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12" style="background-color: white;">
            <div class="card" style="border: 0">
                <img src='{{ asset("img/logo.png") }}' style="background-color: white; width: 50%; margin: 30% 0 10% 26%;">
                <div class="card-header" style="text-align: center; background-color: white; border: 0;" ><strong>{{ __('Нэвтрэх') }}</strong></div>

                <div class="card-body" style="background-color: white;">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <input id="username" placeholder="ERP нэвтрэх нэр" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>

                                @error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span> 
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-3">
                                <input id="password" placeholder="Нууц үг" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-3">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Нэвтрэх') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="card-footer" style="text-align: center; background-color: white; border: 0">
                    <p  class="text-muted">2021 Copyright</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
<style>
    @media only screen and (max-width: 812px) {
    .mobile {
        display: none;
    }
    .col-md-4{
        width: 100%;
    }
    .col-xs-12{
        width: 80%;
    }
}
</style>
@endsection
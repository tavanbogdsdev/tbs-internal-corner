@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="card shadow-sm" style="border: 0">
        <div class="card-body">
            @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div><br />
            @endif
            @if(session()->get('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div><br />
            @endif

            <div class="row">
                <div class="col-md-10">
                    <h5>{{ $document->name}}</h5>
                    <h6 class="mb-2 text-muted">Баримт бичгийн холбогдох маягт</h6>

                </div>
                @if(Auth::user()->role === 'moderator')
                <div class="col-md-2">
                    <button class="btn btn-success btn-sm float-right" type="button" data-toggle="modal" data-target="#exampleModal">
                        <i class="fas fa-plus"></i>
                    </button>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Маягт нэмэх</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="POST" action="{{ url('/document/act') }}" enctype="multipart/form-data">
                                    @csrf
                                    @method('POST')
                                    <div class="modal-body">
                                        <input name="document_id" value="{{ $document->id }}" type="hidden" />

                                        <div class="form-group row">
                                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Нэр') }}</label>

                                            <div class="col-md-6">
                                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="" required autocomplete="name" autofocus />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="number" class="col-md-4 col-form-label text-md-right">{{ __('Дугаар') }}</label>

                                            <div class="col-md-6">
                                                <input id="number" type="text" class="form-control @error('number') is-invalid @enderror" name="number" value="" required autocomplete="number" autofocus />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="version" class="col-md-4 col-form-label text-md-right">{{ __('Хувилбар') }}</label>

                                            <div class="col-md-6">
                                                <input id="version" type="text" class="form-control @error('version') is-invalid @enderror" name="version" value="" required autocomplete="version" autofocus />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="confirmedDate" class="col-md-4 col-form-label text-md-right">{{ __('Батлагдсан, шинэчлэгдсэн огноо') }}</label>

                                            <div class="col-md-6">
                                                <input id="confirmedDate" type="date" class="form-control @error('confirmedDate') is-invalid @enderror" name="confirmedDate" required/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="secret_level" class="col-md-4 col-form-label text-md-right">{{ __('Нууцын түвшин') }}</label>

                                            <div class="col-md-6">
                                                <select id="secret_level" class="custom-select" name="secret_level">
                                                    <option value="Нээлттэй">Нээлттэй</option>
                                                    <option value="Нууц">Нууц</option>
                                                    <option value="Маш нууц">Маш нууц</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="inputGroupFile01" class="col-md-4 col-form-label text-md-right">{{ __('Файл') }}</label>

                                            <div class="col-md-6">
                                                <div class="custom-file">
                                                    <input type="file" class="" id="inputGroupFile01" name="file">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                        <button type="submit" class="btn btn-primary">Хадгалах</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <br>
            <table class="table table-sm table-hover table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th scope="col">Маягтын нэр</th>
                        <th scope="col">Дугаар</th>
                        <th scope="col">Хувилбар</th>
                        <th scope="col">Батлагдсан, шинэчлэгдсэн огноо</th>
                        <th scope="col">Нууцын түвшин</th>
                        @if(Auth::user()->role === 'moderator')
                        <th scope="col"></th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @php($counter=1)
                    @foreach($acts as $act)
                    <tr>
                        <th scope="row">{{ $counter++ }}</th>
                        <td>{{ $act->name }}</td>
                        <td>{{ $act->number }}</td>
                        <td>{{ $act->version }}</td>
                        <td>{{ $act->confirmedDate }}</td>
                        <td>{{ $act->secret_level }}</td>
                        @if(Auth::user()->role === 'moderator')
                        <th scope="col">
                            <a href="#" data-toggle="modal" data-target="#actEdit">
                                <i class="fas fa-pen-fancy"></i>
                            </a>
                            <!-- Modal -->
                            <div class="modal fade" id="actEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Маягт засах</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form method="POST" action="{{ url('/document/act/'. $act->id) }}" enctype="multipart/form-data">
                                            @csrf
                                            @method('PATCH')
                                            <div class="modal-body">
                                                <input name="document_id" value="{{ $document->id }}" type="hidden" />

                                                <div class="form-group row">
                                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Нэр') }}</label>

                                                    <div class="col-md-6">
                                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $act->name }}" required autocomplete="name" autofocus />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="number" class="col-md-4 col-form-label text-md-right">{{ __('Дугаар') }}</label>

                                                    <div class="col-md-6">
                                                        <input id="number" type="text" class="form-control @error('number') is-invalid @enderror" name="number" value="{{ $act->number }}" required autocomplete="number" autofocus />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="version" class="col-md-4 col-form-label text-md-right">{{ __('Хувилбар') }}</label>

                                                    <div class="col-md-6">
                                                        <input id="version" type="text" class="form-control @error('version') is-invalid @enderror" name="version" value="{{ $act->version }}" required autocomplete="version" autofocus />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="confirmedDate" class="col-md-4 col-form-label text-md-right">{{ __('Батлагдсан, шинэчлэгдсэн огноо') }}</label>

                                                    <div class="col-md-6">
                                                        <input id="confirmedDate" type="date" class="form-control @error('confirmedDate') is-invalid @enderror" name="confirmedDate" value="{{ $act->confirmedDate }}" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="secret_level" class="col-md-4 col-form-label text-md-right">{{ __('Нууцын түвшин') }}</label>

                                                    <div class="col-md-6">
                                                        <select id="secret_level" class="custom-select" name="secret_level">
                                                            @if($act->secret_level == 'Нээлттэй')
                                                            <option value="Нээлттэй" selected>Нээлттэй</option>
                                                            @else
                                                            <option value="Нээлттэй">Нээлттэй</option>
                                                            @endif
                                                            @if($act->secret_level == 'Нууц')
                                                            <option value="Нууц" selected>Нууц</option>
                                                            @else
                                                            <option value="Нууц">Нууц</option>
                                                            @endif
                                                            @if($act->secret_level == 'Маш нууц')
                                                            <option value="Маш нууц" selected>Маш нууц</option>
                                                            @else
                                                            <option value="Маш нууц">Маш нууц</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="inputGroupFile01" class="col-md-4 col-form-label text-md-right">{{ __('Файл') }}</label>

                                                    <div class="col-md-6">
                                                        <div class="custom-file">
                                                            <input type="file" class="" id="inputGroupFile01" name="file">
                                                        </div>
                                                        <small style="color: #990000">Хэрэв файл сонгоогүй бол уг маягтын файл өөрчлөгдөхгүй.</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                                <button type="submit" class="btn btn-primary">Хадгалах</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <a href="#" data-toggle="modal" data-target="#deleteActModal{{ $act->id }}" style='color: #FF5412'>
                                <i class="fas fa-trash-alt"></i>
                            </a>
                            <!-- delete Modal -->
                            <div class="modal fade" id="deleteActModal{{ $act->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog  modal-sm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Устгах</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form method="POST" action="{{ url('/document/act/'.$act->id ) }}">
                                            @csrf()
                                            @method('DELETE')
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                                <button type="submit" class="btn btn-primary">Устгах</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </th>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('style')
<style>
    .textclick {
        text-decoration: underline;
        cursor: pointer;
    }

    .textclick:hover {
        color: 'red';
    }

    #toolbarViewerRight {
        display: none;
    }

    #outerContainer #mainContainer div.toolbar {
        display: none !important;
        /* hide PDF viewer toolbar */
    }

    #outerContainer #mainContainer #viewerContainer {
        top: 0 !important;
        /* move doc up into empty bar space */
    }

    .hide {
        display: none;
    }
</style>
@endsection
@section('scripts')
<script>
    // $(document).ready(function() {
    $('.levels-multiple').select2();
    $('.js-example-basic-multiple').select2();
    // });
    $("#fraDisabled").on("contextmenu", function(e) {
        return false;
    });

    function myFunction() {
        $('#iframe').find("#toolbarViewerRight").hide();
        $('#toolbarViewerRight').attr("style", "display: none;");
        $('#print').attr("hidden", "true");
        $("#print").addClass('hidden');
        $("#download").addClass('hidden');

    }
</script>
@endsection
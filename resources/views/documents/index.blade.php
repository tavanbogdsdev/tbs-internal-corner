@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="card shadow-sm" style="border: 0">
        <div class="card-body" style="overflow: auto;">
            @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div><br />
            @endif
            @if(session()->get('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div><br />
            @endif

            <div class="row">
                <div class="col-md-11">
                    <h5>{{ $catname }}</h5>
                </div>

                @if(Auth::user()->role == 'moderator')
                @foreach(Auth::user()->categories as $cate)
                @if($cate->category_id == $category->id)
                <div class="col-md-1">
                    <button class="btn btn-success btn-sm float-right" type="button" data-toggle="modal" data-target="#exampleModal">
                        <i class="fas fa-plus"></i>
                    </button>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Бичиг баримт нэмэх</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="POST" action="{{ route('document.store') }}" enctype="multipart/form-data">
                                    @csrf
                                    @method('POST')
                                    <div class="modal-body">
                                        <input name="category_id" value="{{ $category->id }}" type="hidden" />

                                        @if($category->is_nested)
                                        <div class="form-group row">
                                            <label for="levels" class="col-md-4 col-form-label text-md-right">{{ __('Дэд ангилал') }}</label>

                                            <div class="col-md-6">
                                                <select id="levels" class="custom-select" name="sub_id">
                                                    @foreach($sub_categories as $subcat)
                                                    <option value="{{ $subcat->id }}">{{ $subcat->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        @endif

                                        <div class="form-group row">
                                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Нэр') }}</label>

                                            <div class="col-md-6">
                                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="" required autocomplete="name" autofocus />
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="secrt_level" class="col-md-4 col-form-label text-md-right">{{ __('Нууцын түвшин') }}</label>

                                            <div class="col-md-3">
                                                <select id="secrt_level" class="custom-select" name="secret_level">
                                                    <option value="Нээлттэй">Нээлттэй</option>
                                                    <option value="Нууц">Нууц</option>
                                                    <option value="Маш нууц">Маш нууц</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="number" class="col-md-4 col-form-label text-md-right">{{ __('Дугаар') }}</label>

                                            <div class="col-md-3">
                                                <input id="number" type="text" class="form-control @error('number') is-invalid @enderror" name="number" value="" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="version" class="col-md-4 col-form-label text-md-right">{{ __('Хувилбар') }}</label>

                                            <div class="col-md-3">
                                                <input id="version" type="text" class="form-control @error('version') is-invalid @enderror" name="version" value="" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="confirmedDate" class="col-md-4 col-form-label text-md-right">{{ __('Батлагдсан огноо') }}</label>

                                            <div class="col-md-3">
                                                <input id="confirmedDate" type="date" class="form-control @error('confirmedDate') is-invalid @enderror" name="confirmedDate" required value="" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="owner" class="col-md-4 col-form-label text-md-right">{{ __('Хариуцагч') }}</label>

                                            <div class="col-md-6">
                                                <input id="owner" type="text" class="form-control @error('owner') is-invalid @enderror" name="owner" required value="" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="registration_info" class="col-md-4 col-form-label text-md-right">{{ __('Бүртгэл, баримтжуулсан мэдээлэл') }}</label>

                                            <div class="col-md-6">
                                                <input id="registration_info" type="text" class="form-control @error('registration_info') is-invalid @enderror" name="registration_info" value="" />
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputGroupFile01" class="col-md-4 col-form-label text-md-right">{{ __('Файл') }}</label>

                                            <div class="col-md-6">
                                                <div class="custom-file">
                                                    <input type="file" class="" id="inputGroupFile01" name="file" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="levels" class="col-md-4 col-form-label text-md-right">{{ __('Хандах түвшин') }}</label>

                                            <div class="col-md-6">
                                                <select id="levels" class="multiSelect col-md-12" style="width: 100%" multiple="multiple" name="levels[]" required>
                                                    @foreach($levels as $level)
                                                    <option value="{{ $level->id }}">{{ $level->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group row">
                                            <label for="comment" class="col-md-4 col-form-label text-md-right">{{ __('Тайлбар') }}</label>

                                            <div class="col-md-6">
                                                <input id="comment" type="text" class="form-control @error('comment') is-invalid @enderror" name="comment" value="Шинэ бичиг баримт" />
                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                        <button type="submit" class="btn btn-primary">Хадгалах</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
                @endif
            </div>
            <br>


            <!-- <div class="modal fade" id="documentShow" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-body">
                        
                    </div>
                </div>
            </div> -->

            <div class="modal fade" id="documentView" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" style="overflow: hidden;">
                <div class="modal-dialog modal-dialog-centered" role="document" style="max-width:95vw; height: 80vh;">
                    <div class="modal-content" style="height: 100%;">
                        <div class="modal-header">
                            <h5 class="modal-title" id="documentTitle"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" style="padding: 0; overflow: hidden;" id="pdfdoc">
                            <iframe id="pdfDocument" src="" height="100%" width="100%" frameborder="0" oncontextmenu="return false"></iframe>
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-12">
                    <table class="table table-sm table-hover table-striped">
                        <tbody>
                            @php($counter = 1)
                            @foreach($documents as $item)
                            <tr>
                                <td>{{ $counter++ }}</td>
                                <td class="textclick">
                                    <a href="" id="viewDocument" data-toggle="modal" onclick="viewDocument({loc: '{{$item->path}}', tit: '{{ $item->name }}'})" data-target="#documentView">{{ $item->name }}</a>
                                </td>
                                <!-- -->
                                <!-- <div class="modal fade" id="documentShow{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-body">
                                     
                                        </div>
                                    </div>
                                </div> -->
                                <!-- Modal /my_pdf.pdf?#zoom=85&scrollbar=0&toolbar=0&navpanes=0 -->

                                <!-- <div class="modal fade" id="documentShow{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" style="max-height: 90vh; overflow: hidden;">
                                    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width:95vw; ">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle">{{ $item->name }}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body" style="padding: 0; margin-bottom: -8px; overflow: hidden;" id="pdfdoc">
                                                <div id="my_pdf_viewer">
                                                    <div id="canvas_container">
                                                        <canvas id="pdf_renderer"></canvas>
                                                    </div>
                                                    <div id="navigation_controls">
                                                        <button id="go_previous">Previous</button>
                                                        <input id="current_page" value="1" type="number"/>
                                                        <button id="go_next">Next</button>
                                                    </div>
                                                    <div id="zoom_controls">  
                                                        <button id="zoom_in">+</button>
                                                        <button id="zoom_out">-</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <td class="float-right">{{ $item->number }}</td>
                                <td> {{ $item->version }} </td>
                                <td> {{ $item->registration_info }} </td>
                                <td> {{ $item->confirmedDate }} </td>
                                <td> {{ $item->owner }} </td>
                                <td> {{ $item->secret_level }} </td>
                                <td>
                                    @php( $actc = 0)
                                    @foreach($acts as $act)
                                    @if($act->document_id == $item->id)
                                    @php( $actc = 1)
                                    @if(substr($act->path, -3) == 'mp4')
                                    <!-- Button trigger modal -->
                                    <li class="textclick"><a href="#" data-toggle="modal" data-target="#viewActModal{{ $act->id }}">{{ $act->name }}</a></li>

                                    <!-- Modal -->
                                    <div class="modal fade" id="viewActModal{{ $act->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static">
                                        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLongTitle">{{ $act->name }}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body" style="padding: 0; margin-bottom: -8px">
                                                    <video width="100%" controls class="thumb" data-full="{{ URL::asset('storage/'.$act->path) }}" style="border: 0">
                                                        <source src="{{ URL::asset('storage/'.$act->path) }}">
                                                    </video>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                    <li class="textclick"><a href="{{ url('/act/download/'.$act->id) }}">{{ $act->name }}</a></li>
                                    @endif
                                    @endif
                                    @endforeach
                                    @if($actc == 0)
                                    Байхгүй
                                    @endif
                                </td>
                                @if(Auth::user()->role === 'moderator')
                                @foreach(Auth::user()->categories as $cate)
                                @if($cate->category_id == $category->id)
                                <td>

                                    <a href="{{ url('/document/'.$item->id.'/acts') }}" style='color: #547030'>
                                        <i class="fas fa-paperclip"></i>
                                    </a>
                                    <!-- act Modal -->
                                    <div class="modal fade" id="act{{ $item->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog  modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Холбогдох маягт</h5>
                                                    <button class="btn btn-success btn-sm" type="button" data-toggle="modal" data-target="#addAct" style="margin-left: 20px">
                                                        <i class="fas fa-plus"></i>
                                                    </button>
                                                    <!-- Modal -->
                                                    <div class="modal fade" id="addAct" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog modal-md" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel">Бичиг баримт нэмэх</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <form method="POST" action="{{ url('/document/act') }}" enctype="multipart/form-data">
                                                                    @csrf
                                                                    @method('POST')
                                                                    <div class="modal-body">
                                                                        <input name="document_id" value="{{ $item->id }}" type="hidden" />

                                                                        <div class="form-group row">
                                                                            <label for="name" class="col-md-2 col-form-label text-md-right">{{ __('Нэр') }}</label>

                                                                            <div class="col-md-10">
                                                                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="" required autocomplete="name" autofocus />
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group row">
                                                                            <label for="inputGroupFile01" class="col-md-2 col-form-label text-md-right">{{ __('Файл') }}</label>

                                                                            <div class="col-md-10">
                                                                                <div class="custom-file">
                                                                                    <input type="file" class="" id="inputGroupFile01" name="file" required>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                                                        <button type="submit" class="btn btn-primary">Хадгалах</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table table-sm table-hover table-striped" style="margin: 50px 0 110px 0">
                                                        <tbody>
                                                            @php($act_count = 1)
                                                            @foreach($acts as $act)
                                                            @if($act->document_id == $item->id)
                                                            <tr>
                                                                <td>{{ $act_count++ }}</td>
                                                                <td scope="col">{{ $act->name }}</td>
                                                                <td scope="col">
                                                                    <a href="#" data-toggle="modal" data-target="#deleteActModal{{ $item->id }}" style='color: #FF5412'>
                                                                        <i class="fas fa-trash-alt"></i>
                                                                    </a>
                                                                    <!-- delete Modal -->
                                                                    <div class="modal fade" id="deleteActModal{{ $item->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                        <div class="modal-dialog  modal-sm">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <h5 class="modal-title" id="exampleModalLabel">Устгах</h5>
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                        <span aria-hidden="true">&times;</span>
                                                                                    </button>
                                                                                </div>
                                                                                <form method="POST" action="{{ url('/document/act/'.$act->id ) }}">
                                                                                    @csrf()
                                                                                    @method('DELETE')
                                                                                    <div class="modal-footer">
                                                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                                                                        <button type="submit" class="btn btn-primary">Устгах</button>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </td>
                                                            </tr>
                                                            @endif
                                                            @endforeach
                                                        </tbody>
                                                        <thead>
                                                            <tr>
                                                                <th>#</th>
                                                                <th scope="col">Холбогдох маягтын нэр</th>
                                                                <th scope="col"></th>
                                                            </tr>
                                                        </thead>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <a href="#" data-toggle="modal" data-target="#document{{ $item->id }}" style='color: #543070'>
                                        <i class="fas fa-file-signature"></i>
                                    </a>
                                    <!-- change file of document Modal -->
                                    <div class="modal fade" id="document{{ $item->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog  modal-md">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Баримт бичгийн файл өөрчлөх</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form method="POST" action="{{ url('/document/file/update/'.$item->id ) }}" enctype="multipart/form-data">
                                                    @csrf()
                                                    @method('PATCH')
                                                    <div class="modal-body">
                                                        <div class="form-group row">
                                                            <label for="inputGroupFile01" class="col-md-2 col-form-label text-md-right">{{ __('Файл') }}</label>

                                                            <div class="col-md-10">
                                                                <div class="custom-file">
                                                                    <input type="file" class="" id="inputGroupFile01" name="file" required>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                                        <button type="submit" class="btn btn-primary">Устгах</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                    <a href="#" data-toggle="modal" data-target="#editDepartmentModal{{ $item->id}}">
                                        <i class="fas fa-pen-fancy"></i>
                                    </a>
                                    <!-- edit Modal -->
                                    <div class="modal fade" id="editDepartmentModal{{ $item->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Засах</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form method="POST" action="{{ route('document.update', $item->id) }}" enctype="multipart/form-data">
                                                    @csrf
                                                    @method('PATCH')
                                                    <div class="modal-body">
                                                        <input name="category_id" value="{{ $category->id }}" type="hidden" />

                                                        @if($category->is_nested)

                                                        <div class="form-group row">
                                                            <label for="levels" class="col-md-4 col-form-label text-md-right">{{ __('Дэд ангилал') }}</label>

                                                            <div class="col-md-6">
                                                                <select id="levels" class="custom-select" name="sub_id" required>
                                                                    @foreach($sub_categories as $subcat)
                                                                    @if($subcat->id == $item->sub_id)
                                                                    <option value="{{ $subcat->id }}" selected>{{ $subcat->name }}</option>
                                                                    @endif
                                                                    <option value="{{ $subcat->id }}">{{ $subcat->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        @endif

                                                        <div class="form-group row">
                                                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Нэр') }}</label>

                                                            <div class="col-md-6">
                                                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $item->name}}" required autocomplete="name" autofocus />
                                                            </div>
                                                        </div>

                                                        <div class="form-group row">
                                                            <label for="secrt_level" class="col-md-4 col-form-label text-md-right">{{ __('Нууцын түвшин') }}</label>

                                                            <div class="col-md-3">
                                                                <select id="secrt_level" class="custom-select" name="secret_level">
                                                                    @if($item->secret_level == 'Нээлттэй')
                                                                    <option value="Нээлттэй" selected>Нээлттэй</option>
                                                                    @else
                                                                    <option value="Нээлттэй">Нээлттэй</option>
                                                                    @endif
                                                                    @if($item->secret_level == 'Нууц')
                                                                    <option value="Нууц" selected>Нууц</option>
                                                                    @else
                                                                    <option value="Нууц">Нууц</option>
                                                                    @endif
                                                                    @if($item->secret_level == 'Маш нууц')
                                                                    <option value="Маш нууц" selected>Маш нууц</option>
                                                                    @else
                                                                    <option value="Маш нууц">Маш нууц</option>
                                                                    @endif
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="number" class="col-md-4 col-form-label text-md-right">{{ __('Дугаар') }}</label>

                                                            <div class="col-md-3">
                                                                <input id="number" type="text" class="form-control @error('number') is-invalid @enderror" name="number" value="{{ $item->number }}" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="version" class="col-md-4 col-form-label text-md-right">{{ __('Хувилбар') }}</label>

                                                            <div class="col-md-3">
                                                                <input id="version" type="text" class="form-control @error('version') is-invalid @enderror" name="version" value="{{ $item->version }}" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="confirmedDate" class="col-md-4 col-form-label text-md-right">{{ __('Батлагдсан огноо') }}</label>

                                                            <div class="col-md-3">
                                                                <input id="confirmedDate" type="date" class="form-control @error('confirmedDate') is-invalid @enderror" name="confirmedDate" required value="{{ $item->confirmedDate }}" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="owner" class="col-md-4 col-form-label text-md-right">{{ __('Хариуцагч') }}</label>

                                                            <div class="col-md-6">
                                                                <input id="owner" type="text" class="form-control @error('owner') is-invalid @enderror" name="owner" required value="{{ $item->owner }}" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="registration_info" class="col-md-4 col-form-label text-md-right">{{ __('Бүртгэл, баримтжуулсан мэдээлэл') }}</label>

                                                            <div class="col-md-6">
                                                                <input id="registration_info" type="text" class="form-control @error('registration_info') is-invalid @enderror" name="registration_info" value="{{ $item->registration_info }}" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="levels2" class="col-md-4 col-form-label text-md-right">{{ __('Хандах түвшин') }}</label>
                                                            <div class="col-md-6">
                                                                <p>Hello </p>
                                                                <select id="levels2" data-select2-id="select2-data-levels2" class="multiSelect multiSelectEdit col-md-12" style="width: 100%" multiple="multiple" name="levels[]" required>
                                                                    @foreach($levels as $level)
                                                                    <option value="{{$level->id}}" @foreach($item->levels as $il)
                                                                        @if($level->id == $il->id)
                                                                        selected="selected"
                                                                        @endif
                                                                        @endforeach
                                                                        >
                                                                        {{$level->name}}
                                                                    </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="form-group row">
                                                            <label for="comment" class="col-md-4 col-form-label text-md-right">{{ __('Тайлбар') }}</label>

                                                            <div class="col-md-6">
                                                                <input id="comment" type="text" class="form-control @error('comment') is-invalid @enderror" name="comment" value="Засварлав" />
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                                        <button type="submit" class="btn btn-primary">Хадгалах</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                    <a href="#" data-toggle="modal" data-target="#deleteDepartmentModal{{ $item->id }}" style='color: #FF5412'>
                                        <i class="fas fa-trash-alt"></i>
                                    </a>
                                    <!-- delete Modal -->
                                    <div class="modal fade" id="deleteDepartmentModal{{ $item->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog  modal-sm">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Устгах</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form method="POST" action="{{ route('document.destroy', $item->id ) }}">
                                                    @csrf()
                                                    @method('DELETE')
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                                        <button type="submit" class="btn btn-primary">Устгах</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                @endif
                                @endforeach

                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th scope="col">Баримт бичгийн нэр</th>
                                <th scope="col">Дугаар</th>
                                <th scope="col">Хувилбар</th>
                                <th scope="col">Бүртгэл, баримтжуулсан мэдээлэл</th>
                                <th scope="col">Батлагдсан, шинэчлэгдсэн огноо</th>
                                <th scope="col">Хариуцагч</th>
                                <th scope="col">Нууцын түвшин</th>
                                <th scope="col">Холбогдох маягт</th>
                                @if(Auth::user()->role === 'moderator')
                                @foreach(Auth::user()->categories as $cate)
                                @if($cate->category_id == $category->id)
                                <th scope="col"></th>
                                @endif
                                @endforeach

                                @endif
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('style')
<style>
    .textclick {
        text-decoration: underline;
        cursor: pointer;
    }

    .textclick:hover {
        color: 'red';
    }

    #toolbarViewerRight {
        display: none;
    }

    #outerContainer #mainContainer div.toolbar {
        display: none !important;
        /* hide PDF viewer toolbar */
    }

    #outerContainer #mainContainer #viewerContainer {
        top: 0 !important;
        /* move doc up into empty bar space */
    }

    .hide {
        display: none;
    }
</style>
@endsection
@section('scripts')
<!-- <script src="/vendor/bootstrap/js/bootstrap.min.js"></script> -->
<script>
    var myState = {
        pdf: null,
        currentPage: 1,
        zoom: 1
    }

    function dFile(url, filename) {
        fetch(url).then(function(t) {
            return t.blob().then((b) => {
                var a = document.createElement("a");
                a.href = URL.createObjectURL(b);
                a.setAttribute("download", filename);
                a.click();
            });
        });
    }


    function viewDocument(data) {
        ext = data.loc.substr((data.loc.lastIndexOf('.') + 1));
        if (ext === 'pdf') {
            // $('#documentView').modal('show');
            $('#documentTitle').html(data.tit);
            // $('#pdfDocument').attr('src', `{{URL::asset('test')}}?uri=${data.loc}`);
            $('#pdfDocument').attr('src', `{{URL::asset('pdf/web/viewer.html?file=')}}{{URL::asset('storage')}}/${data.loc}`);

            
        } else {
            dFile(`{{URL::asset('storage/${data.loc}')}}`, data.loc);
            // $('#documentView').modal('hide');
        }

    }

    $(document).ready(function() {
        $('.multiSelect').css("height", "300px");
    });

    // $('#viewDocument').click(function() {
    //     var a = $(this);
    //     var loc = a.data("loc");
    //     var title = a.data("title");
    //     $('#documentTitle').html(title);
    //     $('#pdfDocument').attr('src', `{{URL::asset('test')}}?uri=${loc}`);
    //     // $('#documentTitle').text(title);
    //     // $('#documentView').addClass("show");
    // });

    function render() {
        myState.pdf.getPage(myState.currentPage).then((page) => {
            var canvas = document.getElementById("pdf_renderer");
            var ctx = canvas.getContext('2d');
            var viewport = page.getViewport(myState.zoom);
            canvas.width = viewport.width;
            canvas.height = viewport.height;
            page.render({
                canvasContext: ctx,
                viewport: viewport
            });
        });
    }

    // function myFunction() {
    //     $('#iframe').find("#toolbarViewerRight").hide();
    //     $('#toolbarViewerRight').attr("style", "display: none;");
    //     $('#print').attr("hidden", "true");
    //     $("#print").addClass('hidden');
    //     $("#download").addClass('hidden');

    // }
</script>
@endsection
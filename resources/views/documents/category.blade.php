@extends('layouts.app')

@section('content')
<div class="container-fluid">

    @if(session()->get('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div><br />
    @endif
    @if(session()->get('error'))
    <div class="alert alert-danger">
        {{ session()->get('error') }}
    </div><br />
    @endif

    <div class="row">
        <div class="col-md-11">
            <div class="row">
                @foreach($sub_categories as $sub)
                <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-3">
                    @if($category->is_time_order)
                    <a href="{{ url('/category/'.$category->id.'/events/'.$sub->id.'/'.$sub->name) }}" style="padding: 1%">
                        <div class="card subCard shadow-sm">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-8 col-sm-12 col-md-12 col-lg-8 subCardName">
                                        <span>{{ $sub->name}}</span>
                                    </div>
                                    <div class="col-4 col-sm-12 col-md-12 col-lg-4">
                                        @if($sub->name == 'Дүрэм журмууд')
                                        <img src='{{ asset("img/procedure.png") }}' style="width: 70%; margin-top: 15%;">
                                        @elseif($sub->name == 'Гарын авлага')
                                        <img src='{{ asset("img/file.png") }}' style="width: 70%; margin-top: 15%;">
                                        @elseif($sub->name == 'Заавар, зааварчилгаа')
                                        <img src='{{ asset("img/guide.png") }}' style="width: 70%; margin-top: 15%;">
                                        @elseif($sub->name == '8420УНА')
                                        <img src='{{ asset("img/prius30.png") }}' style="width: 100%; margin-top: 15%;">
                                        @elseif($sub->name == '4218УБИ')
                                        <img src='{{ asset("img/allion.png") }}' style="width: 100%; margin-top: 15%;">
                                        @else
                                        <img src='{{ asset("img/logo_mini.png") }}' style="width: 70%; margin-top: 15%;">
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    @else
                    <a href="{{ url('/category/'.$currentMenu.'/documents/'.$sub->id.'/'.$sub->name) }}" style="padding: 1%">
                        <div class="card subCard shadow-sm">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-8 col-sm-12 col-md-12 col-lg-8 subCardName">
                                        <span>{{ $sub->name}}</span>
                                    </div>
                                    <div class="col-4 col-sm-12 col-md-12 col-lg-4">
                                    @if($sub->name == 'Дүрэм журмууд')
                                        <img src='{{ asset("img/procedure.png") }}' style="width: 70%; margin-top: 15%;">
                                        @elseif($sub->name == 'Гарын авлага')
                                        <img src='{{ asset("img/file.png") }}' style="width: 70%; margin-top: 15%;">
                                        @elseif($sub->name == 'Заавар, зааварчилгаа')
                                        <img src='{{ asset("img/guide.png") }}' style="width: 70%; margin-top: 15%;">
                                        @elseif($sub->name == '8420УНА')
                                        <img src='{{ asset("img/prius30.png") }}' style="width: 100%; margin-top: 15%;">
                                        @elseif($sub->name == '4218УБИ')
                                        <img src='{{ asset("img/allion.png") }}' style="width: 100%; margin-top: 15%;">
                                        @else
                                        <img src='{{ asset("img/logo_mini.png") }}' style="width: 70%; margin-top: 15%;">
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    @endif
                </div>
                @endforeach
            </div>
        </div>
        @if(Auth::user()->role == 'moderator')
        @foreach(Auth::user()->categories as $cate)
        @if($cate->category_id == $category->id)
        <div class="col-md-1">
            <button class="btn btn-success btn-sm float-right" type="button" data-toggle="modal" data-target="#exampleModal">
                <i class="fas fa-plus"></i>
            </button>
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Бичиг баримт нэмэх</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form method="POST" action="{{ route('document.store') }}" enctype="multipart/form-data">
                            @csrf
                            @method('POST')
                            <div class="modal-body">
                                <input name="category_id" value="{{ $category->id }}" type="hidden" />

                                <div class="form-group row">
                                    <label for="levels" class="col-md-4 col-form-label text-md-right">{{ __('Дэд ангилал') }}</label>

                                    <div class="col-md-6">
                                        <select id="levels" class="custom-select" name="sub_id">
                                            @foreach($sub_categories as $subcat)
                                            <option value="{{ $subcat->id }}">{{ $subcat->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Нэр') }}</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="" required autocomplete="name" autofocus />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="secrt_level" class="col-md-4 col-form-label text-md-right">{{ __('Нууцын түвшин') }}</label>

                                    <div class="col-md-3">
                                        <select id="secrt_level" class="custom-select" name="secret_level">
                                            <option value="Нээлттэй">Нээлттэй</option>
                                            <option value="Нууц">Нууц</option>
                                            <option value="Маш нууц">Маш нууц</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="number" class="col-md-4 col-form-label text-md-right">{{ __('Дугаар') }}</label>

                                    <div class="col-md-3">
                                        <input id="number" type="text" class="form-control @error('number') is-invalid @enderror" name="number" value="" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="version" class="col-md-4 col-form-label text-md-right">{{ __('Хувилбар') }}</label>

                                    <div class="col-md-3">
                                        <input id="version" type="text" class="form-control @error('version') is-invalid @enderror" name="version" value="" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="confirmedDate" class="col-md-4 col-form-label text-md-right">{{ __('Батлагдсан огноо') }}</label>

                                    <div class="col-md-3">
                                        <input id="confirmedDate" type="date" class="form-control @error('confirmedDate') is-invalid @enderror" name="confirmedDate" value="" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="owner" class="col-md-4 col-form-label text-md-right">{{ __('Хариуцагч') }}</label>

                                    <div class="col-md-6">
                                        <input id="owner" type="text" class="form-control @error('owner') is-invalid @enderror" name="owner" value="" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="registration_info" class="col-md-4 col-form-label text-md-right">{{ __('Бүртгэл, баримтжуулсан мэдээлэл') }}</label>

                                    <div class="col-md-6">
                                        <input id="registration_info" type="text" class="form-control @error('registration_info') is-invalid @enderror" name="registration_info" value="" />
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputGroupFile01" class="col-md-4 col-form-label text-md-right">{{ __('Файл') }}</label>

                                    <div class="col-md-6">
                                        <div class="custom-file">
                                            <input type="file" class="" id="inputGroupFile01" name="file">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="levels" class="col-md-4 col-form-label text-md-right">{{ __('Хандах түвшин') }}</label>

                                    <div class="col-md-6">
                                        <select id="levels" class="custom-select" multiple="multiple" name="levels[]">
                                            @foreach($levels as $level)
                                            <option value="{{ $level->id }}">{{ $level->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group row">
                                    <label for="comment" class="col-md-4 col-form-label text-md-right">{{ __('Тайлбар') }}</label>

                                    <div class="col-md-6">
                                        <input id="comment" type="text" class="form-control @error('comment') is-invalid @enderror" name="comment" value="Шинэ бичиг баримт" />
                                    </div>
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                <button type="submit" class="btn btn-primary">Хадгалах</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @endforeach
        @endif
    </div>
</div>
@endsection
@section('style')
<style>
    .textclick {
        text-decoration: underline;
        cursor: pointer;
    }

    .textclick:hover {
        color: 'red';
    }

    #toolbarViewerRight {
        display: none;
    }

    #outerContainer #mainContainer div.toolbar {
        display: none !important;
        /* hide PDF viewer toolbar */
    }

    #outerContainer #mainContainer #viewerContainer {
        top: 0 !important;
        /* move doc up into empty bar space */
    }

    .hide {
        display: none;
    }

    .subCardName {
        padding-top: 5%;
        padding-left: 5%;
        font-size: 1rem;
        color: #5A607F
    }

    .subCard {
        border: 0;
        border-radius: 7px;
        min-height: 150px;
    }

    .subCard:hover {
        background-color: #F2F7FF;
        cursor: pointer;
    }
</style>
@endsection
@section('scripts')
<script>
    // $(document).ready(function() {
    $('.levels-multiple').select2();
    $('.js-example-basic-multiple').select2();
    // });
    $("#fraDisabled").on("contextmenu", function(e) {
        return false;
    });

    function myFunction() {
        $('#iframe').find("#toolbarViewerRight").hide();
        $('#toolbarViewerRight').attr("style", "display: none;");
        $('#print').attr("hidden", "true");
        $("#print").addClass('hidden');
        $("#download").addClass('hidden');

    }
</script>
@endsection
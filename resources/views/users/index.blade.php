<!-- index.blade.php -->

@extends('layouts.app')

@section('content')
<!-- <style>
    .uper {
        margin-top: 40px;
    }
</style> -->

<div class="uper container-fluid">
    <div class="card" style="border: 0">
        <div class="card-body">
            @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
            @endif
            @if(session()->get('error'))
            <div class="alert alert-danger">
                {{ session()->get('error') }}
            </div>
            @endif
            <div class="row">
                <div class="col-md-6">
                    <h5>Хэрэглэгчийн жагсаалт</h5>
                </div>

                <div class="col-md-6">
                    <button class="btn btn-success btn-sm float-right" data-toggle="modal" data-target="#userAddModal"> +</button>
                    <!-- add Modal -->
                    <div class="modal fade" id="userAddModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Хэрэглэгч нэмэх</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="POST" action="{{ route('users.store') }}">
                                    <div class="modal-body">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('ERP нэвтрэх нэр') }}</label>

                                            <div class="col-md-6">
                                                <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>

                                                @error('username')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Нэр') }}</label>

                                            <div class="col-md-6">
                                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('Овог') }}</label>

                                            <div class="col-md-6">
                                                <input id="lastname" type="text" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('lastname') }}" required autocomplete="lastname" autofocus>

                                                @error('lastname')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="phone_number" class="col-md-4 col-form-label text-md-right">{{ __('Утаны дугаар') }}</label>

                                            <div class="col-md-6">
                                                <input id="phone_number" type="number" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ old('phone_number') }}" required autocomplete="phone_number" autofocus>

                                                @error('phone_number')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="department_id" class="col-md-4 col-form-label text-md-right">{{ __('Алба, нэгж') }}</label>

                                            <div class="col-md-6">
                                                <select name="department_id" id="department_id" class="form-control @error('department_id') is-invalid @enderror" required>
                                                    @foreach($departments as $dep)
                                                    <option value="{{$dep->id}}">{{$dep->name}}</option>
                                                    @endforeach
                                                </select>

                                                @error('department_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="job_id" class="col-md-4 col-form-label text-md-right">{{ __('Албан тушаал') }}</label>

                                            <div class="col-md-6">
                                                <select name="job_id" id="job_id" class="form-control @error('job_id') is-invalid @enderror" required>
                                                    @foreach($jobs as $job)
                                                    <option value="{{$job->id}}">{{$job->name}}</option>
                                                    @endforeach
                                                </select>

                                                @error('job_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="level_id" class="col-md-4 col-form-label text-md-right">{{ __('Түвшин') }}</label>

                                            <div class="col-md-6">
                                                <select name="level_id" id="level_id" class="form-control @error('level_id') is-invalid @enderror" required>
                                                    @foreach($levels as $level)
                                                    <option value="{{$level->id}}">{{$level->name}}</option>
                                                    @endforeach
                                                </select>

                                                @error('level_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="role_id" class="col-md-4 col-form-label text-md-right">{{ __('Хандах эрх') }}</label>

                                            <div class="col-md-6">
                                                <select name="role_id" id="role_id" class="form-control @error('role_id') is-invalid @enderror" required>
                                                    @foreach($roles as $role)
                                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                                    @endforeach
                                                </select>

                                                @error('role_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="categories_id" class="col-md-4 col-form-label text-md-right">{{ __('Категори') }}</label>

                                            <div class="col-md-6">
                                                <select name="categories[]" multiple="multiple" id="categories_id" class="form-control @error('categories_id') is-invalid @enderror" required>
                                                    <option value="0"></option>
                                                    @foreach($categories_all as $item)
                                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                                    @endforeach
                                                </select>
                                                <small style="color: #980000">Хандах эрх moderator бол категори сонгоно уу.</small>

                                                @error('categories_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Нууц үг') }}</label>

                                            <div class="col-md-6">
                                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                                @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Нууц үг давтах') }}</label>

                                            <div class="col-md-6">
                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                        <button type="submit" class="btn btn-primary">Нэмэх</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>

            </div>
            <br>
            <table class="table table-sm table-hover table-striped">
                <thead>
                    <tr>
                        <td>#</td>
                        <td>M/ID</td>
                        <td>ERP нэр</td>
                        <td>Овог</td>
                        <td>Нэр</td>
                        <td>Утасны дугаар</td>
                        <td>Алба нэгж</td>
                        <td>Албан тушаал</td>
                        <td>Түвшин</td>
                        <td>Хандах эрх</td>
                        <td>Модератор</td>
                        <td colspan="2"></td>
                    </tr>
                </thead>
                <tbody>
                    @php($countUser = 1)
                    @foreach($users as $user)
                    <tr>
                        <td>{{$countUser++}}</td>
                        <td>{{$user->machine_id}}</td>
                        <td>{{$user->username}}</td>
                        <td>{{$user->lastname}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->phone_number}}</td>
                        <td>{{$user->department}}</td>
                        <td>{{$user->job}}</td>
                        <td>{{$user->level}}</td>
                        <td>{{$user->role}}</td>
                        <td>
                            @foreach($user->categories as $uc)
                            <li>{{ $uc->name }}</li>
                            @endforeach
                        </td>
                        <td>
                            <a href="#" data-toggle="modal" data-target="#userResetModal{{$user->id}}" style="color: #853445" alt="Нууц үг reset"><i class="fas fa-retweet"></i></a>
                            <!-- Reset Modal -->
                            <div class="modal fade" id="userResetModal{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Нууц үг RESET</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form method="POST" action="{{ url('/user/password/reset/'. $user->id) }}">
                                            <div class="modal-body">
                                                @csrf
                                                <div class="form-group row">
                                                    <label for="name" class="col-md-12 col-form-label text-md-right">{{ __('RESET: qwerty123') }}</label>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                                <button type="submit" class="btn btn-primary">RESET</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <a href="{{ route('users.edit',$user->id)}}" data-toggle="modal" data-target="#userEditModal{{$user->id}}"><i class="fas fa-pen-fancy"></i></a>
                            <!-- Modal -->
                            <div class="modal fade" id="userEditModal{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Хэрэглэгч засах</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form method="POST" action="{{ route('users.update', $user->id) }}">
                                            <div class="modal-body">
                                                @csrf
                                                @method('PATCH')

                                                <div class="form-group row">
                                                    <label for="machine_id" class="col-md-4 col-form-label text-md-right">{{ __('Машины ID') }}</label>

                                                    <div class="col-md-6">
                                                        <input id="machine_id" type="text" class="form-control @error('machine_id') is-invalid @enderror" name="machine_id" value="{{ $user->machine_id }}" required autocomplete="machine_id" autofocus>

                                                        @error('lastname')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="username" class="col-md-4 col-form-label text-md-right">{{ __('ERP нэвтрэх нэр') }}</label>

                                                    <div class="col-md-6">
                                                        <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ $user->username }}" required autocomplete="username" autofocus>

                                                        @error('username')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                

                                                <div class="form-group row">
                                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Нэр') }}</label>

                                                    <div class="col-md-6">
                                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}" required autocomplete="name" autofocus>

                                                        @error('name')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('Овог') }}</label>

                                                    <div class="col-md-6">
                                                        <input id="lastname" type="text" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ $user->lastname }}" required autocomplete="lastname" autofocus>

                                                        @error('lastname')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="phone_number" class="col-md-4 col-form-label text-md-right">{{ __('Утаны дугаар') }}</label>

                                                    <div class="col-md-6">
                                                        <input id="phone_number" type="number" class="form-control @error('phone_number') is-invalid @enderror" name="phone_number" value="{{ $user->phone_number }}" required autocomplete="phone_number" autofocus>

                                                        @error('phone_number')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="department_id" class="col-md-4 col-form-label text-md-right">{{ __('Алба, нэгж') }}</label>

                                                    <div class="col-md-6">
                                                        <select name="department_id" id="department_id" class="form-control @error('department_id') is-invalid @enderror" required>
                                                            @foreach($departments as $dep)
                                                            @if($dep->id == $user->department_id)
                                                            <option value="{{$dep->id}}" selected>{{$dep->name}}</option>
                                                            @else
                                                            <option value="{{$dep->id}}">{{$dep->name}}</option>
                                                            @endif
                                                            @endforeach
                                                        </select>

                                                        @error('department_id')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="job_id" class="col-md-4 col-form-label text-md-right">{{ __('Албан тушаал') }}</label>

                                                    <div class="col-md-6">
                                                        <select name="job_id" id="job_id" class="form-control @error('job_id') is-invalid @enderror" required>
                                                            @foreach($jobs as $job)
                                                            @if($job->id == $user->job_id)
                                                            <option value="{{$job->id}}" selected>{{$job->name}}</option>
                                                            @else
                                                            <option value="{{$job->id}}">{{$job->name}}</option>
                                                            @endif
                                                            @endforeach
                                                        </select>

                                                        @error('job_id')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="level_id" class="col-md-4 col-form-label text-md-right">{{ __('Түвшин') }}</label>

                                                    <div class="col-md-6">
                                                        <select name="level_id" id="level_id" class="form-control @error('level_id') is-invalid @enderror" required>
                                                            @foreach($levels as $level)
                                                            @if($level->id == $user->level_id)
                                                            <option value="{{$level->id}}" selected>{{$level->name}}</option>
                                                            @else
                                                            <option value="{{$level->id}}">{{$level->name}}</option>
                                                            @endif
                                                            @endforeach
                                                        </select>

                                                        @error('level_id')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="role_id" class="col-md-4 col-form-label text-md-right">{{ __('Хандах эрх') }}</label>

                                                    <div class="col-md-6">
                                                        <select name="role_id" id="role_idUpdate" class="form-control @error('role_id') is-invalid @enderror" required>
                                                            @foreach($roles as $role)
                                                            @if($role->id == $user->role_id)
                                                            <option value="{{$role->id}}" selected>{{$role->name}}</option>
                                                            @else
                                                            <option value="{{$role->id}}">{{$role->name}}</option>
                                                            @endif
                                                            @endforeach
                                                        </select>

                                                        @error('role_id')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="categories_id" class="col-md-4 col-form-label text-md-right">{{ __('Категори') }}</label>
                                                    <?php
                                                    $cats_list = [];
                                                    foreach ($categories_all as $cate) {
                                                        foreach ($user->categories as $c) {
                                                            if ($cate->id === $c->id) {
                                                                $cate->isSelect = true;
                                                                break;
                                                            } else {
                                                                $cate->isSelect = false;
                                                            }
                                                        }
                                                        array_push($cats_list, $cate);
                                                    }

                                                    ?>
                                                    <div class="col-md-6">
                                                        <select name="categories[]" multiple="multiple" id="categories_idUpdate" class="form-control @error('categories_id') is-invalid @enderror" required>
                                                            @if(count($user->categories) != 0)
                                                            <option value="0">none</option>
                                                            @foreach($cats_list as $kat)
                                                            @if($kat->isSelect)
                                                            <option value="{{$kat->id}}" selected="selected">{{$kat->name}}</option>
                                                            @else
                                                            <option value="{{$kat->id}}">{{$kat->name}}</option>
                                                            @endif
                                                            @endforeach
                                                            @else
                                                            <option value="0">none</option>
                                                            @foreach($categories_all as $kat)
                                                            <option value="{{$kat->id}}">{{$kat->name}}</option>
                                                            @endforeach
                                                            @endif
                                                        </select>
                                                        <small style="color: #980000">Хандах эрх moderator бол категори сонгоно уу.</small>

                                                        @error('categories_id')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Цуцлах</button>
                                                <button type="submit" class="btn btn-primary">Хадгалах</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </td>
                        <td>
                            <form action="{{ route('users.destroy', $user->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button style='color: #FF5412' type="submit"><i class="fas fa-trash-alt"></i></button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div>
                <div>
                    <div>
                        @endsection
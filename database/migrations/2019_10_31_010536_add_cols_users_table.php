<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('lastname')->nullable();
            $table->string('phone_number')->nullable();
            $table->integer('department_id')->nullable();
            $table->foreign('department_id')->references('id')->on('departments');
            $table->integer('job_id')->nullable();
            $table->foreign('job_id')->references('id')->on('jobs');
            $table->integer('level_id')->nullable();
            $table->foreign('level_id')->references('id')->on('levels');
            $table->integer('role_id')->nullable();
            $table->foreign('role_id')->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}

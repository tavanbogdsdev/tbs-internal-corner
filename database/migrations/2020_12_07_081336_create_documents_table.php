<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('path');
            $table->string('version')->nullable();
            $table->date('confirmedDate');
            $table->string('owner');
            $table->string('registration_info')->nullable();
            $table->integer('category_id');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->string('number')->nullable();
            $table->string('secret_level')->nullable();
            $table->integer('sub_id')->nullable();
            $table->foreign('sub_id')->references('id')->on('sub_category');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents_levels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('document_id');
            $table->foreign('document_id')->references('id')->on('documents');
            $table->integer('levels_id');
            $table->foreign('levels_id')->references('id')->on('levels');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents_levels');
    }
}

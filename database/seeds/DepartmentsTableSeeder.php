<?php

use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            'name' => 'Захиргаа хүний нөөцийн алба'
        ]);
        DB::table('departments')->insert([
            'name' => 'Борлуулалт хөгжүүлэлтийн алба'
        ]);
        DB::table('departments')->insert([
            'name' => 'Засвар үйлчилгээний алба'
        ]);
        DB::table('departments')->insert([
            'name' => 'Цэнэглэлтийн алба'
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class JobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jobs')->insert([
            'name' => 'Хүний нөөцийн менежер'
        ]);
        DB::table('jobs')->insert([
            'name' => 'Хүний нөөцийн мэргэжилтэн'
        ]);
        DB::table('jobs')->insert([
            'name' => 'ХАБЭА-Механик инженер'
        ]);
        DB::table('jobs')->insert([
            'name' => 'Ерөнхий нягтлан бодогч'
        ]);
        DB::table('jobs')->insert([
            'name' => 'Санхүүгийн шинжээч'
        ]);
        DB::table('jobs')->insert([
            'name' => 'Нярав'
        ]);
        DB::table('jobs')->insert([
            'name' => 'Мэдээлэл технологи, ISO инженер'
        ]);
        DB::table('jobs')->insert([
            'name' => 'МАБУТ/ Гадаад харилцааны мэргэжилтэн '
        ]);
        DB::table('jobs')->insert([
            'name' => 'Борлуулалт хөгжүүлэлтийн албаны ахлах менежер'
        ]);
        DB::table('jobs')->insert([
            'name' => 'Хөгжүүлэлтийн мэргэжилтэн'
        ]);
        DB::table('jobs')->insert([
            'name' => 'Хөгжүүлэлтийн инженер'
        ]);
        DB::table('jobs')->insert([
            'name' => 'Хяналтын систем хариуцсан менежер'
        ]);
        DB::table('jobs')->insert([
            'name' => 'Ахлах оператор'
        ]);
        DB::table('jobs')->insert([
            'name' => 'Хяналтын оператор'
        ]);
        DB::table('jobs')->insert([
            'name' => 'Салбар хариуцсан ахлах менежер '
        ]);
        DB::table('jobs')->insert([
            'name' => 'Ахлах инженер'
        ]);
        DB::table('jobs')->insert([
            'name' => 'Сэлбэг засварын инженер'
        ]);
        DB::table('jobs')->insert([
            'name' => 'АТМ хариуцсан инженер '
        ]);
        DB::table('jobs')->insert([
            'name' => 'Жолооч '
        ]);
        DB::table('jobs')->insert([
            'name' => 'Цэнэглэлтийн албаны ахлах менежер'
        ]);
        DB::table('jobs')->insert([
            'name' => 'Цэнэглэлтийн ахлах инженер'
        ]);
        DB::table('jobs')->insert([
            'name' => 'Цэнэглэлтийн инженер'
        ]);
    }
}

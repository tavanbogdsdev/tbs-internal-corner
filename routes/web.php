<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    if(is_object(Auth::user())) {
        return redirect('/');
    }
});
Auth::routes();
Route::get('/register', 'HomeController@index')->name('home');
Route::get('/document/{id}/acts', 'DocumentController@showActs')->middleware('role:^admin');

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user/password/change', 'UsersController@changeForm');
Route::post('/user/password/change', 'UsersController@change');
Route::post('/user/password/reset/{id}', 'UsersController@reset')->middleware('role:^admin');
Route::resource('users', 'UsersController')->middleware('role:^admin');
Route::resource('document', 'DocumentController')->middleware('role:^admin');
Route::get('/category/{id}', 'CategoryController@show')->middleware('auth');
Route::get('/category/{catid}/documents/{id}/{catname}', 'CategoryController@showDocuments')->middleware('auth');
Route::patch('/document/act/{id}', 'DocumentController@updateAct')->middleware('role:^admin');

Route::resource('setup', 'SettingsController')->middleware('role:^admin');
Route::get('/settings', 'SettingsController@index')->middleware('role:^admin')->name('settings');
Route::resource('category', 'CategoryController')->middleware('role:^admin');
Route::resource('sub_category', 'SubCategoryController')->middleware('role:^admin');
Route::resource('level', 'LevelController')->middleware('role:^admin');
Route::resource('department', 'DepartmentController')->middleware('role:^admin');
Route::resource('job', 'JobController')->middleware('role:^admin');

Route::patch('/document/file/update/{id}', 'DocumentController@changefile')->middleware('role:^admin')->name('changefile');
Route::post('/document/act', 'DocumentController@storeAct')->middleware('role:^admin');
Route::delete('/document/act/{id}', 'DocumentController@deleteAct')->middleware('role:^admin');
Route::get('/act/download/{id}', 'DocumentController@downloadAct');

Route::get('/category/{id}/events/{eid}/{catname}', 'EventController@index');
Route::post('/event/create', 'EventController@store');
Route::get('/event/delete/{id}', 'EventController@destroy');

Route::get('/test', function (Request $request) {
    print_r(Auth::user());
});
